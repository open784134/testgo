package video

import (
	"encoding/json"
	"fmt"

	"wukong/lib"
	数据库 "wukong/model"
	交通数据库 "wukong/model/traffic"
)

func 统计流量(scheme_id, frame_num, fps int, period int, device_type, laneNumber int) {
	// t0 := time.Now().UnixNano() / 1e6

	var 查询结果 交通数据库.StructTrafficTarget
	endtime := lib.Frame2Time(frame_num, fps)
	starttime := lib.TimeCompute(endtime, -10, fps)
	sql := fmt.Sprintf(`select id_auto, id_stream, id_defination, id_lane, target_id, target_type, 
	created_at from traffic_target where id_stream= '%d' and created_at >= '%s' and created_at < '%s' and device_type='%d' and id_defination='%d' order by created_at desc`,
		Stream_id, starttime, endtime, device_type, scheme_id)
	rows := 数据库.QueryTraffic(sql)

	// t3 := time.Now().UnixNano() / 1e6
	// fmt.Println("断面统计1------------------- ", t3-t0)

	var objectData []map[string]interface{}
	for rows.Next() {
		err := rows.StructScan(&查询结果)
		if err != nil {
			fmt.Println("统计流量: ", err)
		}
		dataMap := make(map[string]interface{})
		dataMap["type"] = 查询结果.TargetType
		dataMap["createdAt"] = 查询结果.CreatedAt
		dataMap["idLane"] = 查询结果.IdLane
		objectData = append(objectData, dataMap)
	}

	// 生成content数据
	var timeDataAll []map[string]interface{}
	//var DataAll []map[string]interface{} //筛选计算过后的时间粒度的总数据
	timeSelectData := make(map[string]interface{})
	lanenum := int(laneNumber) + 1
	for n := 0; n < lanenum; n++ {
		flow := fmt.Sprintf("flow%d", n)
		timeSelectData[flow] = 0
		midsmallbus := fmt.Sprintf("midsmallbus%d", n)
		timeSelectData[midsmallbus] = 0
		bus := fmt.Sprintf("bus%d", n)
		timeSelectData[bus] = 0
		truck := fmt.Sprintf("truck%d", n)
		timeSelectData[truck] = 0
		person := fmt.Sprintf("person%d", n)
		timeSelectData[person] = 0
		bicycle := fmt.Sprintf("bicycle%d", n)
		timeSelectData[bicycle] = 0
	}
	timeDataAll = append(timeDataAll, timeSelectData)
	// 时区
	//Loc, _ := time.LoadLocation("Asia/Shanghai")
	// fmt.Println("========", objectData)
	for v := range timeDataAll {
		for i := range objectData {
			// 目标时间
			//to, _ := time.Parse("2006-01-02T15:04:05Z", objectData[i]["createdAt"].(string))
			//stamp := to.Format("2006-01-02 15:04:05")
			//dt, _ := time.ParseInLocation("2006-01-02 15:04:05", stamp, Loc)
			// 开始时间和结束时间时间戳
			//timeStampStart, _ := time.ParseInLocation("2006-01-02 15:04:05", timeDataAll[v]["beforeTime"].(string), Loc)
			//timeStampEnd, _ := time.ParseInLocation("2006-01-02 15:04:05", timeDataAll[v]["afterTime"].(string), Loc)
			// 满足时间区间的数据
			//if dt.Unix() > timeStampStart.Unix() && dt.Unix() < timeStampEnd.Unix() {
			lanenum := int(laneNumber) + 1
			for n := 0; n < lanenum; n++ {
				if objectData[i]["idLane"].(int) == n {
					// fmt.Println(objectData)
					if objectData[i]["type"].(string) == "小客车" {
						str := fmt.Sprintf("midsmallbus%d", n)
						result := timeDataAll[v][str].(int) + 1
						timeDataAll[v][str] = result
					} else if objectData[i]["type"].(string) == "大客车" {
						str := fmt.Sprintf("bus%d", n)
						result := timeDataAll[v][str].(int) + 1
						timeDataAll[v][str] = result
					} else if objectData[i]["type"].(string) == "货车" {
						str := fmt.Sprintf("truck%d", n)
						result := timeDataAll[v][str].(int) + 1
						timeDataAll[v][str] = result
					} else if objectData[i]["type"].(string) == "非机动车" {
						str := fmt.Sprintf("bicycle%d", n)
						result := timeDataAll[v][str].(int) + 1
						timeDataAll[v][str] = result
					} else if objectData[i]["type"].(string) == "行人" {
						str := fmt.Sprintf("person%d", n)
						result := timeDataAll[v][str].(int) + 1
						timeDataAll[v][str] = result
					}
				}
			}
			//}
		}
		// 计算流量总和
		lanenum := int(laneNumber) + 1
		for n := 0; n < lanenum; n++ {
			midsmallbus := fmt.Sprintf("midsmallbus%d", n)
			bus := fmt.Sprintf("bus%d", n)
			truck := fmt.Sprintf("truck%d", n)
			person := fmt.Sprintf("person%d", n)
			bicycle := fmt.Sprintf("bicycle%d", n)
			flow := fmt.Sprintf("flow%d", n)
			timeDataAll[v][flow] = timeDataAll[v][midsmallbus].(int) + timeDataAll[v][bus].(int) +
				timeDataAll[v][truck].(int) + timeDataAll[v][person].(int) + timeDataAll[v][bicycle].(int)
		}
	}
	// 去除无用数据
	/*
		for i := range timeDataAll {
			lanenum := int(lanenumber) + 1
			for n := 1; n < lanenum; n++ {
				flow := fmt.Sprintf("flow%d", n)
				timeDataAll[i]["flow0"] = timeDataAll[i]["flow0"].(int) + timeDataAll[i][flow].(int)
			}
			if timeDataAll[i]["flow0"] != 0 {
				DataAll = append(DataAll, timeDataAll[i])
			}
		}*/
	dataType, _ := json.Marshal(timeDataAll[0])
	dataString := string(dataType)

	// t4 := time.Now().UnixNano() / 1e6
	// fmt.Println("断面统计2------------------- ", t4-t3)
	var created_time string
	if frame_num >= Max_frame_num {
		// created_time = lib.Frame2Time(Max_frame_num, fps)
		created_time = videoEndTime
	} else {
		created_time = lib.Frame2Time(frame_num, fps)
	}
	insertSql := fmt.Sprintf(`insert into traffic_target_stat(id_stream, id_defination, period, content, device_type, created_at,frame) 
	values ('%d', '%d', '%d', '%s', '%d','%s',%d)`, Stream_id, scheme_id, period, dataString, device_type, created_time, frame_num)
	数据库.ExecTraffic(insertSql)

	// t5 := time.Now().UnixNano() / 1e6
	// fmt.Println("断面统计3------------------- ", t5-t4)

	if frame_num%(fps*60) == 0 {
		定时计算流量(frame_num, fps, 60, 10, device_type)
		if frame_num%(fps*600) == 0 {
			定时计算流量(frame_num, fps, 600, 60, device_type)
			if frame_num%(fps*3600) == 0 {
				定时计算流量(frame_num, fps, 3600, 600, device_type)
				// if frame_num%(fps*3600) == 0 {
				// 	定时计算流量(86400, 3600, device_type)
				// }
			}
		}
	}
	// t6 := time.Now().UnixNano() / 1e6
	// fmt.Println("断面统计总------------------- ", t6-t0)

}

func 定时计算流量(frame_num, fps int, period int, last_period int, device_type int) {
	// t1 := time.Now().UnixNano() / 1e6
	var 查询结果 交通数据库.StructTrafficTargetStat
	flowNum := make(map[string]int)
	var eachFlow map[string]int
	// rows_num := period / last_period
	endtime := lib.Frame2Time(frame_num, fps)
	starttime := lib.TimeCompute(endtime, -period, fps)

	sql := fmt.Sprintf(`select id_auto, id_stream, id_defination, period, content,
	created_at from traffic_target_stat where id_stream = '%d' and period = '%d' and device_type='%d' and id_defination='%d' and created_at > '%s' order by created_at desc`,
		Stream_id, last_period, device_type, scheme_id, starttime)
	rows := 数据库.QueryTraffic(sql)
	for rows.Next() {
		err := rows.StructScan(&查询结果)
		if err != nil {
			fmt.Println(err)
		}
		// fmt.Println("comtemt-------------", 查询结果.Content)
		json.Unmarshal([]byte(查询结果.Content), &eachFlow)
		for k, v := range eachFlow {
			// fmt.Println("++++++++++++ ", k, flowNum[k], v)
			flowNum[k] = flowNum[k] + v
			// fmt.Println("************ ", k, flowNum[k], v)
		}
		// q, _ := json.Marshal(flowNum)
		// fmt.Println("result------------------ ", string(q))
	}
	dataType, _ := json.Marshal(flowNum)
	dataString := string(dataType)
	var created_time string
	if frame_num >= Max_frame_num {
		// created_time = lib.Frame2Time(Max_frame_num, fps)
		created_time = videoEndTime
	} else {
		created_time = lib.Frame2Time(frame_num, fps)
	}
	insertSql := fmt.Sprintf(`insert into traffic_target_stat(id_stream, id_defination, period, content, device_type, created_at,frame) 
	values ('%d', '%d', '%d', '%s', '%d', '%s',%d)`, Stream_id, scheme_id, period, dataString, device_type, created_time, frame_num)
	数据库.ExecTraffic(insertSql)

	// t2 := time.Now().UnixNano() / 1e6
	// fmt.Println("断面---- ", period, t2-t1)
}
