package radar

import (
	"math"
	"math/rand"
	"strconv"
	"sync"
	"time"
	"wukong/controller/video"
	"wukong/model/holo"
	"wukong/strcture"

	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-base/matrices"
	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-traffic/trajectory/azimuth"
	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-traffic/trajectory/smoother"
	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-traffic/trajectory/speed"

	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-base/cache"
	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-base/uuid2id"

	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-base/cmp"
	"codeup.aliyun.com/645b14d64fbe916d80fa5509/lanjian-gomod-base/traffic"

	"go.uber.org/zap"
	"golang.org/x/exp/slices"
	"gonum.org/v1/gonum/mat"
)

// TODO: 改为传递变量
const (
	fusionRadarFPS           = 20
	fusionVideoFPS           = 25
	fusionRadarFrameInterval = time.Second / fusionRadarFPS
	fusionVideoFrameInterval = time.Second / fusionVideoFPS
)

// trajectoryFaker 用于生成假轨迹。
type trajectoryFaker interface {
	// Next 返回下一个假轨迹点。
	Next() (strcture.Car_inf, bool)
}

// uniformTrajectoryFaker 生成匀速直线运动的假轨迹。
type uniformTrajectoryFaker struct {
	target strcture.Car_inf
	vx     float64
	vy     float64
}

func newUniformTrajectoryFaker(target strcture.Car_inf, hardcodeFakeSpeed, fps, azimuth float64) *uniformTrajectoryFaker {
	if hardcodeFakeSpeed != 0.0 {
		target.Speed = hardcodeFakeSpeed
	}
	speedMpf := target.Speed / 3.6 / fps
	return &uniformTrajectoryFaker{
		target: target,
		vx:     speedMpf * math.Sin(azimuth),
		vy:     speedMpf * math.Cos(azimuth),
	}
}

func (f *uniformTrajectoryFaker) Next() (strcture.Car_inf, bool) {
	f.target.X += f.vx
	f.target.Y += f.vy
	return f.target, true
}

// leftRightTurnTrajectoryFaker 生成左右转弯的假轨迹。
type leftRightTurnTrajectoryFaker struct {
	logger     *zap.Logger
	target     strcture.Car_inf
	dirFactor  float64
	fps        float64
	thetaX     float64
	vAlign     float64
	vVertical  float64
	a          float64
	frames     int64
	frameLimit int64
}

var _ trajectoryFaker = (*leftRightTurnTrajectoryFaker)(nil)

func newLeftRightTurnTrajectoryFaker(logger *zap.Logger, target strcture.Car_inf, hardcodeFakeSpeed, dirFactor, fps, destX, destY, destRadian float64) *leftRightTurnTrajectoryFaker {
	if hardcodeFakeSpeed != 0.0 {
		ratio := hardcodeFakeSpeed / target.Speed
		target.V_x *= ratio
		target.V_y *= ratio
		target.Speed = hardcodeFakeSpeed
	}

	xDiff := destX - target.X
	yDiff := destY - target.Y
	d := math.Sqrt(xDiff*xDiff + yDiff*yDiff)
	thetaX := math.Atan2(yDiff, xDiff)
	vx := target.V_x / 3.6
	vy := target.V_y / 3.6
	speed := target.Speed / 3.6

	// 原速度向量与位移向量的夹角
	cosTheta := (vx*xDiff + vy*yDiff) / speed / d
	sinTheta := math.Sqrt(1 - cosTheta*cosTheta)
	vAlign := speed * cosTheta
	vVertical := speed * sinTheta
	destVVertical := vAlign * math.Tan(dirFactor*(destRadian-thetaX))

	secs := d / vAlign
	a := (destVVertical - vVertical) / secs

	logger.Debug("Created new leftRightTurnTrajectoryFaker",
		zap.Int("id", target.Id),
		zap.String("deviceID", target.DeviceID),
		zap.Float64("x", target.X),
		zap.Float64("y", target.Y),
		zap.Float64("azimuth", target.Radian*180/math.Pi),
		zap.Float64("dirFactor", dirFactor),
		zap.Float64("destX", destX),
		zap.Float64("destY", destY),
		zap.Float64("destAzimuth", destRadian*180/math.Pi),
		zap.Float64("d", d),
		zap.Float64("thetaX", thetaX*180/math.Pi),
		zap.Float64("vx", vx),
		zap.Float64("vy", vy),
		zap.Float64("cosTheta", cosTheta),
		zap.Float64("sinTheta", sinTheta),
		zap.Float64("vAlign", vAlign),
		zap.Float64("vVertical", vVertical),
		zap.Float64("destVVertical", destVVertical),
		zap.Float64("secs", secs),
		zap.Float64("a", a),
	)

	return &leftRightTurnTrajectoryFaker{
		logger:     logger,
		target:     target,
		dirFactor:  dirFactor,
		fps:        fps,
		thetaX:     thetaX,
		vAlign:     vAlign,
		vVertical:  vVertical,
		a:          a,
		frameLimit: int64(math.Abs(secs) * fps),
	}
}

func (f *leftRightTurnTrajectoryFaker) vVerticalT() float64 {
	return f.vVertical + f.a*float64(f.frames)/f.fps
}

func (f *leftRightTurnTrajectoryFaker) vxt() float64 {
	return f.vAlign*math.Cos(f.thetaX) + f.vVerticalT()*math.Cos(f.thetaX+f.dirFactor*math.Pi/2)
}

func (f *leftRightTurnTrajectoryFaker) vyt() float64 {
	return f.vAlign*math.Sin(f.thetaX) + f.vVerticalT()*math.Sin(f.thetaX+f.dirFactor*math.Pi/2)
}

func (f *leftRightTurnTrajectoryFaker) Next() (strcture.Car_inf, bool) {
	f.frames++
	vx := f.vxt()
	vy := f.vyt()
	f.target.V_x = vx * 3.6
	f.target.V_y = vy * 3.6
	f.target.Speed = math.Sqrt(f.target.V_x*f.target.V_x + f.target.V_y*f.target.V_y)
	f.target.X += vx / f.fps
	f.target.Y += vy / f.fps

	f.logger.Debug("leftRightTurnTrajectoryFaker.Next",
		zap.Int("id", f.target.Id),
		zap.Int64("frames", f.frames),
		zap.Float64("vx", vx),
		zap.Float64("vy", vy),
		zap.Float64("speed", f.target.Speed/3.6),
		zap.Float64("x", f.target.X),
		zap.Float64("y", f.target.Y),
	)

	return f.target, f.frames <= f.frameLimit
}

// leftRightTurnTrajectoryFaker2 生成左右转弯的假轨迹。
type leftRightTurnTrajectoryFaker2 struct {
	target strcture.Car_inf
	curve  []traffic.PointXY
	index  int
}

var _ trajectoryFaker = (*leftRightTurnTrajectoryFaker2)(nil)

func newLeftRightTurnTrajectoryFaker2(logger *zap.Logger, target strcture.Car_inf, hardcodeFakeSpeed, fps, destX, destY float64) *leftRightTurnTrajectoryFaker2 {
	if hardcodeFakeSpeed != 0.0 {
		ratio := hardcodeFakeSpeed / target.Speed
		target.V_x *= ratio
		target.V_y *= ratio
		target.Speed = hardcodeFakeSpeed
	}

	xDiff := destX - target.X
	yDiff := destY - target.Y
	d := math.Sqrt(xDiff*xDiff + yDiff*yDiff)
	vx := target.V_x / 3.6
	vy := target.V_y / 3.6
	speed := target.Speed / 3.6
	cosTheta := (vx*xDiff + vy*yDiff) / speed / d
	vAlign := speed * cosTheta
	secs := d / vAlign
	frames := int64(math.Abs(secs) * fps)
	curve := curveFrom2Points(traffic.PointXY{X: target.X, Y: target.Y}, traffic.PointXY{X: destX, Y: destY}, int(frames))

	logger.Debug("Created new leftRightTurnTrajectoryFaker2",
		zap.Int("id", target.Id),
		zap.String("deviceID", target.DeviceID),
		zap.Float64("x", target.X),
		zap.Float64("y", target.Y),
		zap.Float64("azimuth", target.Radian*180/math.Pi),
		zap.Float64("destX", destX),
		zap.Float64("destY", destY),
		zap.Float64("d", d),
		zap.Float64("vx", vx),
		zap.Float64("vy", vy),
		zap.Float64("cosTheta", cosTheta),
		zap.Float64("vAlign", vAlign),
		zap.Float64("secs", secs),
		zap.Any("curve", curve),
	)

	return &leftRightTurnTrajectoryFaker2{
		target: target,
		curve:  curve,
	}
}

func (f *leftRightTurnTrajectoryFaker2) Next() (strcture.Car_inf, bool) {
	if f.index >= len(f.curve) {
		return f.target, false
	}
	f.target.X = f.curve[f.index].X
	f.target.Y = f.curve[f.index].Y
	f.index++
	return f.target, true
}

func curveFrom2Points(p1, p2 traffic.PointXY, num int) []traffic.PointXY {
	// 计算中间点的位置
	midX := (p1.X + p2.X) / 2
	midY := (p1.Y + p2.Y) / 2

	// 计算斜率
	k := (p2.Y - p1.Y) / (p2.X - p1.X)

	// 计算曲线上的一系列点
	curve := make([]traffic.PointXY, num+1)
	for i := range curve {
		x := p1.X + (float64(i)/10)*(p2.X-p1.X)
		y := cubicSpline(x, p1.X, p1.Y, p2.X, p2.Y, midX, midY, k)
		curve[i] = traffic.PointXY{X: x, Y: y}
	}
	return curve
}

func cubicSpline(x, x1, y1, x2, y2, xm, ym, k float64) float64 {
	h1 := x - x1
	h2 := x2 - x

	// 计算插值多项式的系数
	a := ((h1 * h1 * h1) - (h1 * h2 * h2)) / (h1 * h1 * h2 * h2)
	b := ((h2 * h2 * h2) - (h2 * h1 * h1)) / (h1 * h2 * h2 * h2)
	c := (3*h1*ym + k - 3*y1) / h1 * h1
	d := (3*y2 - k - 3*ym*h2) / h2 * h2

	// 计算插值多项式的值
	return a*math.Pow(x-x1, 3) + b*math.Pow(x2-x, 3) + c*(x-x1) + d*(x2-x)
}

// leftRightTurnTrajectoryFaker3 生成左右转弯的假轨迹。
type leftRightTurnTrajectoryFaker3 struct {
	faker1    *leftRightTurnTrajectoryFaker
	migration *migrationState
	k         float64
	b         float64
}

var _ trajectoryFaker = (*leftRightTurnTrajectoryFaker3)(nil)

func newLeftRightTurnTrajectoryFaker3(logger *zap.Logger, target strcture.Car_inf, hardcodeFakeSpeed, dirFactor, fps, destX, destY, destRadian float64) *leftRightTurnTrajectoryFaker3 {
	if hardcodeFakeSpeed != 0.0 {
		ratio := hardcodeFakeSpeed / target.Speed
		target.V_x *= ratio
		target.V_y *= ratio
		target.Speed = hardcodeFakeSpeed
	}

	xDiff := destX - target.X
	yDiff := destY - target.Y
	k := yDiff / xDiff
	b := target.Y - k*target.X
	d := math.Sqrt(xDiff*xDiff + yDiff*yDiff)
	thetaX := math.Atan2(yDiff, xDiff)
	vx := target.V_x / 3.6
	vy := target.V_y / 3.6
	speed := target.Speed / 3.6

	// 原速度向量与位移向量的夹角
	cosTheta := (vx*xDiff + vy*yDiff) / speed / d
	sinTheta := math.Sqrt(1 - cosTheta*cosTheta)
	vAlign := speed * cosTheta
	vVertical := speed * sinTheta
	destVVertical := vAlign * math.Tan(dirFactor*(destRadian-thetaX))

	secs := d / vAlign
	a := (destVVertical - vVertical) / secs
	frameLimit := math.Abs(secs) * fps

	logger.Debug("Created new leftRightTurnTrajectoryFaker",
		zap.Int("id", target.Id),
		zap.String("deviceID", target.DeviceID),
		zap.Float64("x", target.X),
		zap.Float64("y", target.Y),
		zap.Float64("azimuth", target.Radian*180/math.Pi),
		zap.Float64("dirFactor", dirFactor),
		zap.Float64("destX", destX),
		zap.Float64("destY", destY),
		zap.Float64("destAzimuth", destRadian*180/math.Pi),
		zap.Float64("d", d),
		zap.Float64("thetaX", thetaX*180/math.Pi),
		zap.Float64("vx", vx),
		zap.Float64("vy", vy),
		zap.Float64("cosTheta", cosTheta),
		zap.Float64("sinTheta", sinTheta),
		zap.Float64("vAlign", vAlign),
		zap.Float64("vVertical", vVertical),
		zap.Float64("destVVertical", destVVertical),
		zap.Float64("secs", secs),
		zap.Float64("a", a),
	)

	return &leftRightTurnTrajectoryFaker3{
		faker1: &leftRightTurnTrajectoryFaker{
			logger:     logger,
			target:     target,
			dirFactor:  dirFactor,
			fps:        fps,
			thetaX:     thetaX,
			vAlign:     vAlign,
			vVertical:  vVertical,
			a:          a,
			frameLimit: int64(frameLimit),
		},
		migration: &migrationState{
			step:       1.0 / frameLimit,
			prevWeight: 1.0,
		},
		k: k,
		b: b,
	}
}

func (f *leftRightTurnTrajectoryFaker3) Next() (strcture.Car_inf, bool) {
	target, ok := f.faker1.Next()
	if !ok {
		return target, false
	}

	x := target.X
	y := f.k*x + f.b
	f.migration.prevPoint = traffic.PointXY{X: target.X, Y: target.Y}
	f.migration.nextPoint = traffic.PointXY{X: x, Y: y}
	pt := f.migration.point()
	target.X = pt.X
	target.Y = pt.Y
	return target, true
}

type migrationState struct {
	prevID     int
	nextID     int
	prevPoint  traffic.PointXY
	nextPoint  traffic.PointXY
	step       float64
	prevWeight float64
}

func newMigrationState(target0, target1 *targetInfo, fps float64) *migrationState {
	return &migrationState{
		prevID:     target0.Id,
		nextID:     target1.Id,
		prevPoint:  traffic.PointXY{X: target0.X, Y: target0.Y},
		nextPoint:  traffic.PointXY{X: target1.X, Y: target1.Y},
		step:       1.0 / fps,
		prevWeight: 1,
	}
}

func (m *migrationState) update(logger *zap.Logger, target *targetInfo) {
	switch target.Id {
	case m.prevID:
		m.prevPoint.X = target.X
		m.prevPoint.Y = target.Y
	case m.nextID:
		m.nextPoint.X = target.X
		m.nextPoint.Y = target.Y
	default:
		logger.Debug("Invalid target ID, target class may have changed",
			zap.Int("targetID", target.Id),
			zap.Int("targetClass", target.Cls),
			zap.Int("prevID", m.prevID),
			zap.Int("nextID", m.nextID),
		)
	}
}

func (m *migrationState) point() traffic.PointXY {
	if m.prevWeight != 0 {
		m.prevWeight -= m.step
		if m.prevWeight < 0 {
			m.prevWeight = 0
		}
	}
	return traffic.PointXY{
		X: m.prevPoint.X*m.prevWeight + m.nextPoint.X*(1-m.prevWeight),
		Y: m.prevPoint.Y*m.prevWeight + m.nextPoint.Y*(1-m.prevWeight),
	}
}

type targetFusionInfo struct {
	// fusedID 保存该目标的融合 ID。
	fusedID int

	// laneID 保存该目标所在车道的 ID。
	laneID int

	// laneLandmark 保存该目标所在车道的地标信息。
	laneLandmark holo.Landmark

	// hidden 控制是否隐藏该目标不显示，只用作数据处理。
	hidden bool

	// plate 保存该目标的车牌信息。
	plate strcture.PlateData

	// points 在全向拼接时保存该目标的所有点。
	// 全向拼接时将认定为单个目标的所有点加进来。
	// apply 时会取平均点位应用，然后清空该 slice。
	// 分入口道拼接时不使用，但仍可能有值。
	points []traffic.PointXY

	// migration 保存该目标的迁移状态。
	migration *migrationState
}

func (i *targetFusionInfo) avgPoint() (x, y float64) {
	for _, p := range i.points {
		x += p.X
		y += p.Y
	}
	x /= float64(len(i.points))
	y /= float64(len(i.points))
	return
}

func (i *targetFusionInfo) applyToTarget(logger *zap.Logger, target *strcture.Car_inf) bool {
	target.UID = i.fusedID
	target.PlateNumber = i.plate.PlateNumber
	target.PlateColor = i.plate.Color
	target.Vehicle_type = i.plate.VehicleType

	if i.migration != nil {
		i.points = i.points[:0]
		return i.applyMigrationToTarget(logger, target)
	}

	if len(i.points) == 0 {
		return false
	}
	target.X, target.Y = i.avgPoint()
	i.points = i.points[:0]
	return !i.hidden
}

func (i *targetFusionInfo) applyMigrationToTarget(logger *zap.Logger, target *strcture.Car_inf) bool {
	switch target.Id {
	case i.migration.prevID:
		return false
	case i.migration.nextID:
		xy := i.migration.point()
		target.X = xy.X
		target.Y = xy.Y
		return true
	default:
		logger.Debug("Invalid target ID, target class may have changed",
			zap.Int("targetID", target.Id),
			zap.Int("targetClass", target.Cls),
			zap.Int("fusedID", i.fusedID),
			zap.Int("prevID", i.migration.prevID),
			zap.Int("nextID", i.migration.nextID),
		)
		return false
	}
}

// FusionManager 管理多路雷达轨迹数据的融合。
type FusionManager struct {
	mu                         sync.Mutex
	logger                     *zap.Logger
	platesCh                   <-chan []strcture.PlateData
	fps                        float64
	frameInterval              time.Duration
	hardcodeFakeSpeed          float64
	excludedTargetIDs          []int64
	isRadar                    bool
	debugAddCalibrationPoints  bool
	hideNonMotorVehicles       bool
	debugDisableFusion         bool
	enableFakeHappyEyeballs    bool
	speedIsMps                 bool
	disableTrajectorySmoothing bool
	cachedPlates               []strcture.PlateData
	targetFusionInfoCache      *cache.Unbounded[int, *targetFusionInfo]
	targetSmootherCache        *cache.KeyedReusablePool[int, *smoother.Smoother]
	targetAzimuthCache         *cache.KeyedReusablePool[int, *azimuth.Calculator]
	producerMap                map[<-chan []strcture.Car_inf]*targetSpliceSet
	u2id                       *uuid2id.UUID2ID
}

const (
	defaultSmoothWindowSize         = 5
	defaultAzimuthDistanceThreshold = 2.5
	speedFrames                     = 15 // 此前是 15
	prevSpeedWeight                 = 0.2
)

// NewFusionManager 创建 FusionManager 实例。
func NewFusionManager(
	logger *zap.Logger,
	platesCh <-chan []strcture.PlateData,
	fps float64,
	frameInterval time.Duration,
	hardcodeFakeSpeed float64,
	excludedTargetIDs []int64,
	smoothWindowSize int,
	azimuthDistanceThreshold float64,
	isRadar, debugAddCalibrationPoints,
	hideNonMotorVehicles, debugDisableFusion,
	enableFakeHappyEyeballs, speedIsMps,
	disableTrajectorySmoothing bool,
) *FusionManager {
	if smoothWindowSize == 0 {
		smoothWindowSize = defaultSmoothWindowSize
	}
	if azimuthDistanceThreshold == 0.0 {
		azimuthDistanceThreshold = defaultAzimuthDistanceThreshold
	}
	return &FusionManager{
		logger:                     logger,
		platesCh:                   platesCh,
		fps:                        fps,
		frameInterval:              frameInterval,
		hardcodeFakeSpeed:          hardcodeFakeSpeed,
		excludedTargetIDs:          excludedTargetIDs,
		isRadar:                    isRadar,
		debugAddCalibrationPoints:  debugAddCalibrationPoints,
		hideNonMotorVehicles:       hideNonMotorVehicles,
		debugDisableFusion:         debugDisableFusion,
		enableFakeHappyEyeballs:    enableFakeHappyEyeballs,
		speedIsMps:                 speedIsMps,
		disableTrajectorySmoothing: disableTrajectorySmoothing,
		producerMap:                make(map[<-chan []strcture.Car_inf]*targetSpliceSet),
		targetFusionInfoCache:      cache.NewUnbounded[int, *targetFusionInfo](time.Minute),
		targetSmootherCache: cache.NewKeyedReusablePool[int, *smoother.Smoother](func() *smoother.Smoother {
			return smoother.NewSmoother(smoothWindowSize)
		}, time.Minute),
		targetAzimuthCache: cache.NewKeyedReusablePool[int, *azimuth.Calculator](func() *azimuth.Calculator {
			return azimuth.NewCalculator(int(fps), azimuthDistanceThreshold)
		}, time.Minute),
		u2id: uuid2id.New(time.Minute),
	}
}

// RegisterProducer 注册生产者。
func (fm *FusionManager) RegisterProducer(ch <-chan []strcture.Car_inf) {
	fm.mu.Lock()
	fm.producerMap[ch] = &targetSpliceSet{
		TargetSpeedCache: cache.NewKeyedReusablePool[int, *speed.Calculator](func() *speed.Calculator {
			return speed.NewCalculator(speedFrames, fm.frameInterval, prevSpeedWeight)
		}, time.Minute),
		EventInfo: cache.NewUnbounded[int, *targetEventInfo](3 * time.Minute),
	}
	fm.mu.Unlock()
}

// UnregisterProducer 注销生产者。
func (fm *FusionManager) UnregisterProducer(ch <-chan []strcture.Car_inf) {
	fm.mu.Lock()
	delete(fm.producerMap, ch)
	fm.mu.Unlock()
}

const plateCacheDuration = 10 * time.Second

func (fm *FusionManager) updateCachedPlates() {
	now := time.Now()

	// 移除过期车牌
	newStartIndex := 0
	for i, plate := range fm.cachedPlates {
		if now.Sub(plate.Time) > plateCacheDuration {
			newStartIndex = i + 1
		} else {
			break
		}
	}
	copy(fm.cachedPlates, fm.cachedPlates[newStartIndex:])
	fm.cachedPlates = fm.cachedPlates[:len(fm.cachedPlates)-newStartIndex]

	// 塞新车牌进来
	for {
		select {
		case plates := <-fm.platesCh:
			for i := range plates {
				plates[i].Time = now
			}
			fm.cachedPlates = append(fm.cachedPlates, plates...)
		default:
			return
		}
	}
}

func (fm *FusionManager) convertRadarTargetCoordinates(section *holo.RoadSectionConfig, targets []strcture.Car_inf) {
	calibration := section.Calibration.Data()

	for i := range targets {
		target := &targets[i]
		target.RawX, target.RawY, target.RawLaneID = target.X, target.Y, uint8(target.IdLane)
		target.X, target.Y = holo.MapToCoordinateSystem(target.X, target.Y, calibration.OppositeSectionRotationAngleSin, calibration.OppositeSectionRotationAngleCos, calibration.DeviceLocation[0], calibration.DeviceLocation[1])
	}
}

func (fm *FusionManager) convertVideoTargetCoordinates(section *holo.RoadSectionConfig, targets []strcture.Car_inf) {
	calibration := section.Calibration.Data()
	var h mat.Dense
	h.SetRawMatrix(calibration.Video.Pixel2WorldHoMat)

	for i := range targets {
		target := &targets[i]
		pixelM := mat.NewDense(1, 2, []float64{target.Coo_x, target.Coo_y})
		worldM, err := matrices.HomographyTransform(&h, pixelM)
		if err != nil {
			fm.logger.Warn("Failed to transform video target coordinates",
				zap.Uint64("radarSectionID", section.ID),
				zap.Int("targetID", target.Id),
				zap.Error(err),
			)
			continue
		}
		target.X, target.Y = worldM.At(0, 0), worldM.At(0, 1)
	}
}

func (fm *FusionManager) buildTargetFusionInfoCache(intersection *holo.IntersectionConfig, producerTargetSpliceSet *targetSpliceSet) {
	section := producerTargetSpliceSet.RadarSection
	targets := producerTargetSpliceSet.Targets
	oppositeSection := producerTargetSpliceSet.EntrySection
	oppositeSectionEnterData := producerTargetSpliceSet.EntrySection.EnterData.Data()
	oppositeSectionEnterLines := oppositeSectionEnterData.Lines
	oppositeSectionEnterLaneNumber := oppositeSectionEnterData.LaneNumber

	for i := range targets {
		target := &targets[i]

		// 当前车道地标
		var landmark holo.Landmark
		enterLineIndex := target.IdLane - 1
		if enterLineIndex >= 0 && enterLineIndex < len(oppositeSectionEnterLines) {
			landmark = oppositeSectionEnterLines[enterLineIndex].Landmark
		}

		tfi := fm.getOrCreateTargetFusionInfo(intersection, oppositeSection, target, landmark)

		// 车牌只匹配机动车
		if !target.IsMotorVehicle() {
			continue
		}

		if fm.isRadar && tfi.plate.PlateNumber == "" && target.RawY < oppositeSection.EntryWayYEnd+5 {
			// 匹配一波车牌
			fm.logger.Debug("Vehicle entered opposite section plate fusion zone",
				zap.Uint64("radarSectionID", section.ID),
				zap.Int("cachedPlates", len(fm.cachedPlates)),
				zap.Int("targetID", target.Id),
				zap.Int("targetLaneID", target.IdLane),
				zap.Float64("targetX", target.X),
				zap.Float64("targetY", target.Y),
				zap.Float64("targetRawX", target.RawX),
				zap.Float64("targetRawY", target.RawY),
			)

			for j, plate := range fm.cachedPlates {
				fm.logger.Debug("Matching plate",
					zap.Uint64("radarSectionID", section.ID),
					zap.String("plateNumber", plate.PlateNumber),
					zap.String("plateCameraIPv4", plate.CameraIPv4),
					zap.String("oppositeSectionCameraIPv4", oppositeSection.RoadSectionInternalConfig.CameraIPv4),
					zap.Uint8("plateLaneID", plate.LaneID),
					zap.Int("targetLaneID", target.IdLane),
				)

				// 匹配当前路段车
				if plate.CameraIPv4 != oppositeSection.RoadSectionInternalConfig.CameraIPv4 {
					continue
				}

				// 相机是从最左侧入口道开始从 1 编号，跟雷达相反，这里需要转换下
				if radarLaneIDFromVideoLaneID(plate.LaneID, oppositeSectionEnterLaneNumber) != uint8(tfi.laneID) {
					continue
				}

				tfi.plate = plate
				fm.cachedPlates = slices.Delete(fm.cachedPlates, j, j+1)
				fm.logger.Debug("Matched plate",
					zap.Uint64("radarSectionID", section.ID),
					zap.String("plateNumber", plate.PlateNumber),
					zap.String("plateCameraIPv4", plate.CameraIPv4),
					zap.String("oppositeSectionCameraIPv4", oppositeSection.RoadSectionInternalConfig.CameraIPv4),
					zap.Uint8("plateLaneID", plate.LaneID),
					zap.Int("targetLaneID", target.IdLane),
				)
				break
			}
		}
	}
}

func radarLaneIDFromVideoLaneID(videoLaneID, oppositeSectionEnterLaneNumber uint8) uint8 {
	return oppositeSectionEnterLaneNumber + 2 - videoLaneID
}

// 匆忙放这的函数，基于重构后的那个 ray 函数改写，适配 float64。
func ZonePointsContain(ap [][2]float64, px, py float64) bool {
	flag := false
	sum := 0.0

	for i, j := 0, len(ap)-1; i < len(ap); i, j = i+1, i {
		sx := ap[i][0]
		sy := ap[i][1]
		tx := ap[j][0]
		ty := ap[j][1]
		sum += sx + sy + tx + ty

		if sx == px && sy == py || tx == px && ty == py {
			return true
		}

		if sy < py && py <= ty || ty < py && py <= sy {
			x := sx + (py-sy)*(tx-sx)/(ty-sy)
			if x == px {
				return true
			}
			if x > px {
				flag = !flag
			}
		}
	}

	if !flag || sum == 0.0 {
		return false
	}
	return true
}

func (fm *FusionManager) getOrCreateTargetFusionInfo(intersection *holo.IntersectionConfig, entrySection *holo.RoadSectionConfig, target *targetInfo, laneLandmark holo.Landmark) *targetFusionInfo {
	tfi, ok := fm.targetFusionInfoCache.Get(target.Id)
	if !ok {
		tfi = &targetFusionInfo{
			fusedID:      target.Id,
			laneID:       target.IdLane,
			laneLandmark: laneLandmark,
			// 符合下列任意条件的目标隐藏:
			// - 雷达目标出现时不在路段内, 且不属于任何进口道
			// - 视频目标出现时不在路段内
			hidden: fm.isRadar && target.RawY < entrySection.EntryWayYEnd && !entrySection.IsEntryLane(target.RawLaneID) ||
				!fm.isRadar && !entrySection.IsBehindStopLine(target.X, target.Y),
		}
		fm.targetFusionInfoCache.Set(target.Id, tfi)
	} else {
		if target.IdLane == 255 {
			target.IdLane = tfi.laneID
		} else {
			tfi.laneID = target.IdLane
		}
	}
	target.fusion = tfi
	xy := traffic.PointXY{X: target.X, Y: target.Y}
	tfi.points = append(tfi.points, xy)
	if tfi.migration != nil {
		tfi.migration.update(fm.logger, target)
	}
	return tfi
}

func (fm *FusionManager) mergeTargetFusionInfo(car0, car1 *targetInfo) *targetFusionInfo {
	if car0.fusion == car1.fusion { // 早先已经有轨迹点同时融合了 car0 和 car1
		return car0.fusion
	}
	car0.spliced = true
	car1.spliced = true

	points := append(car0.fusion.points, car1.fusion.points...)

	var (
		overwriteTarget *targetInfo
		tfi             *targetFusionInfo
	)

	switch {
	// 谁有车牌就选谁
	case car0.fusion.plate.PlateNumber != "":
		overwriteTarget = car1
		tfi = car0.fusion
	case car1.fusion.plate.PlateNumber != "":
		overwriteTarget = car0
		tfi = car1.fusion
	// 都没车牌，先到先得
	case car0.fusion.fusedID < car1.fusion.fusedID:
		overwriteTarget = car1
		tfi = car0.fusion
	default:
		overwriteTarget = car0
		tfi = car1.fusion
	}

	tfi.points = points
	tfi.hidden = car0.fusion.hidden && car1.fusion.hidden
	overwriteTarget.fusion = tfi
	fm.targetFusionInfoCache.Set(overwriteTarget.Id, tfi)
	return tfi
}

type targetInfo struct {
	strcture.Car_inf
	fusion       *targetFusionInfo
	faker        trajectoryFaker
	isFinalFaker bool // 最终假轨迹，不准再换 faker 了
	spliced      bool
}

type targetSpliceSet struct {
	RadarSection                *holo.RoadSectionConfig
	EntrySection                *holo.RoadSectionConfig
	LeftTurnEntrySection        *holo.RoadSectionConfig
	RightTurnEntrySection       *holo.RoadSectionConfig
	Targets                     []targetInfo
	PrevTargets                 []targetInfo
	TargetSpeedCache            *cache.KeyedReusablePool[int, *speed.Calculator]
	EventInfo                   *cache.Unbounded[int, *targetEventInfo]
	EventInfoUpdateTime         time.Time
	NextCongestionDetectionTime time.Time
	OverflowCount               int
	OverflowEmitted             bool
	AvgQueueDelay               time.Duration
	AvgStationaryDuration       time.Duration
	FrameID                     int
	FlowCount                   int
	FlowSec                     int
}

// calculateSpeed 仅限视频计算速度。雷达速度由雷达数据直接提供。
func (s *targetSpliceSet) calculateSpeed() {
	for i := range s.Targets {
		target := &s.Targets[i]
		vx, vy, speed, radian, ok := s.TargetSpeedCache.GetOrCreate(target.Id).Calculate(int64(s.FrameID), traffic.PointXY{X: target.X, Y: target.Y})
		target.V_x = vx
		target.V_y = vy
		target.Speed = speed
		if !ok {
			continue
		}
		target.Radian = radian * 180 / math.Pi // TODO: 当前为了兼容前端，实际存的角度，以后应改为弧度
	}
}

func (s *targetSpliceSet) spliceByDirection(sets []*targetSpliceSet, intersection *holo.IntersectionConfig, fm *FusionManager) {
	for i := range s.Targets {
		car0 := &s.Targets[i]
		// - 非进口道出现的车 (隐藏车), 只被动匹配
		// - 已经匹配上，不再重复匹配
		// - 不在匹配区域内的车，不匹配
		if car0.fusion.hidden || car0.fusion.migration != nil || !isInSplicingZone(car0.X, car0.Y, intersection.SplicingZoneRadius) {
			continue
		}

		matchLineConfig := s.EntrySection.MatchLineConfig.Data()
		exitDirection := matchLineConfig.Match(car0.X, car0.Y, car0.fusion.laneLandmark, s.EntrySection.Direction)
		if exitDirection == holo.DirectionUnspecified {
			continue
		}

		for _, set := range sets {
			if exitDirection != set.EntrySection.Direction {
				continue
			}
			for j := range set.Targets {
				car1 := &set.Targets[j]
				if car1.fusion.hidden && shouldSplice(car0, car1, intersection.SpliceAsSameTargetSpeedDiffKmph, intersection.SpliceAsSameTargetDistance) {
					tfi := fm.mergeTargetFusionInfo(car0, car1)
					tfi.migration = newMigrationState(car0, car1, fm.fps)
					break
				}
			}
		}
	}
}

func (s *targetSpliceSet) splice(remainingSets []*targetSpliceSet, intersection *holo.IntersectionConfig, fm *FusionManager) {
	for _, set := range remainingSets {
		for i := range s.Targets {
			car0 := &s.Targets[i]
			// 机动车分入口道匹配，只有非机动车进行综合匹配
			if car0.spliced || car0.IsMotorVehicle() || !isInSplicingZone(car0.X, car0.Y, intersection.SplicingZoneRadius) {
				continue
			}
			for j := range set.Targets {
				car1 := &set.Targets[j]
				if shouldSplice(car0, car1, intersection.SpliceAsSameTargetSpeedDiffKmph, intersection.SpliceAsSameTargetDistance) {
					fm.mergeTargetFusionInfo(car0, car1)
					break
				}
			}
		}
	}
}

// isInSplicingZone 判断车辆是否在融合区域内。
// 这里使用矩形区域，因为更适合路口场景。
func isInSplicingZone(x, y, radius float64) bool {
	return math.Abs(x) <= radius && math.Abs(y) <= radius
}

func shouldSplice(car0, car1 *targetInfo, spliceAsSameTargetSpeedDiffKmph, spliceAsSameTargetDistance float64) bool {
	return car0.IsMotorVehicle() == car1.IsMotorVehicle() &&
		math.Abs(car0.Speed-car1.Speed) < spliceAsSameTargetSpeedDiffKmph &&
		math.Sqrt(math.Pow(car0.X-car1.X, 2)+math.Pow(car0.Y-car1.Y, 2)) < spliceAsSameTargetDistance
}

type targetEventInfo struct {
	// 车辆驶入进口道的时间
	EntryWayEnterTime time.Time

	// 车辆驶出进口道的时间
	EntryWayExitTime time.Time

	// 排队延误 = 驶出时间 - 驶入时间 - 最短通行时间
	QueueDelay time.Duration

	// 静止开始时间
	StationaryStartTime time.Time

	// 静止时长
	StationaryDuration time.Duration
}

func (s *targetSpliceSet) getOrCreateTargetEventInfo(id int) *targetEventInfo {
	tei, ok := s.EventInfo.Get(id)
	if !ok {
		tei = &targetEventInfo{}
		s.EventInfo.Set(id, tei)
	}
	return tei
}

const stationarySpeedThresholdKmph = 5.0

func (s *targetSpliceSet) processTargetEvent(target *strcture.Car_inf) {
	minPassDuration := time.Duration((s.EntrySection.EntryWayYStart - s.EntrySection.EntryWayYEnd) / 1000.0 / s.EntrySection.FreeStreamSpeed * 3600.0 * float64(time.Second))
	oppositeSectionEnterData := s.EntrySection.EnterData.Data()
	tei := s.getOrCreateTargetEventInfo(target.Id)
	s.EventInfoUpdateTime = time.UnixMilli(target.Time)

	// 判定（机动车）在进口道需满足：
	// - 不在非机动车道（车道号为 0）
	// - 在进口道内（车道号小于等于进口道车道数）
	// - 原始雷达 Y 坐标在配置的区间内
	isInEntryWay := target.RawLaneID != 0 && target.RawLaneID <= oppositeSectionEnterData.LaneNumber &&
		s.EntrySection.EntryWayYEnd <= target.RawY && target.RawY <= s.EntrySection.EntryWayYStart

	switch {
	case isInEntryWay && tei.EntryWayEnterTime.IsZero():
		// 驶入进口道，记录驶入时间
		tei.EntryWayEnterTime = time.UnixMilli(target.Time)
	case !isInEntryWay && !tei.EntryWayEnterTime.IsZero() && tei.EntryWayExitTime.IsZero():
		// 驶出进口道，记录驶出时间，计算排队延误
		tei.EntryWayExitTime = time.UnixMilli(target.Time)
		tei.QueueDelay = tei.EntryWayExitTime.Sub(tei.EntryWayEnterTime) - minPassDuration
	}

	isStationary := target.Speed < stationarySpeedThresholdKmph

	if isStationary {
		// 在出口道入口处静止，增加溢流计数
		if ZonePointsContain(s.EntrySection.OverflowAreaPoints, target.X, target.Y) {
			s.OverflowCount++
		}

		// 静止开始，记录静止开始时间
		if tei.StationaryStartTime.IsZero() {
			tei.StationaryStartTime = time.UnixMilli(target.Time)
		}
	} else if !tei.StationaryStartTime.IsZero() {
		// 静止结束，记录静止时长
		tei.StationaryDuration = time.UnixMilli(target.Time).Sub(tei.StationaryStartTime)
		tei.StationaryStartTime = time.Time{}
	}
}

const overflowCountThreshold = 3

func (s *targetSpliceSet) emitEvents(logger *zap.Logger) {
	if s.OverflowCount >= overflowCountThreshold {
		if !s.OverflowEmitted {
			s.OverflowEmitted = true
			video.EmitRadarEvent(int(s.RadarSection.ID), 1, "溢流", strcture.Car_inf{
				DeviceID: s.RadarSection.RadarDeviceID,
			})
		}
	} else {
		s.OverflowEmitted = false
	}
	s.OverflowCount = 0

	if s.EventInfoUpdateTime.Before(s.NextCongestionDetectionTime) {
		return
	}
	s.NextCongestionDetectionTime = s.EventInfoUpdateTime.Add(3 * time.Minute)

	var count int
	teis := s.EventInfo.Values()
	for _, tei := range teis {
		if tei.QueueDelay == 0 {
			continue
		}
		count++
		s.AvgQueueDelay += tei.QueueDelay
		s.AvgStationaryDuration += tei.StationaryDuration
	}
	if count == 0 {
		return
	}
	s.AvgQueueDelay /= time.Duration(count)
	s.AvgStationaryDuration /= time.Duration(count)

	logger.Info("Calculated flow stats",
		zap.Int("section", int(s.RadarSection.ID)),
		zap.Duration("avgQueueDelay", s.AvgQueueDelay),
		zap.Duration("avgStationaryDuration", s.AvgStationaryDuration),
	)
}

const (
	fakeSpeedThresholdKmph = 5.0
	fakeXThreshold         = 250.0
	fakeYThreshold         = 150.0
)

func (fm *FusionManager) spliceTargets(intersection *holo.IntersectionConfig, sortedSets []*targetSpliceSet, targetListCapacityHint int) []strcture.Car_inf {
	splicedTargets := make([]strcture.Car_inf, 0, targetListCapacityHint)

	for i, set := range sortedSets {
		var defusedTargets []strcture.Car_inf // 去融合的目标，用于交通流计算和事件检测
		maxMotorLaneID := set.EntrySection.EnterData.Data().LaneNumber + set.EntrySection.OutData.Data().LaneNumber + 1

		if fm.enableFakeHappyEyeballs {
			// 查找在当前帧消失的目标，进行造假
		iterPrev:
			for i := range set.PrevTargets {
				prevTarget := &set.PrevTargets[i]

				// - 已融合的目标不造假
				// - 隐藏的目标不造假
				// - 速度过小的目标不造假
				if prevTarget.spliced || prevTarget.fusion.hidden || prevTarget.Speed < fakeSpeedThresholdKmph {
					continue
				}

				for j := range set.Targets {
					target := &set.Targets[j]
					if target.Id == prevTarget.Id {
						continue iterPrev
					}
				}

				// 第一次造假，根据入口道类型创建造假器
				if prevTarget.faker == nil {
					// 过滤掉头和噪点
					if !set.EntrySection.IsPastHalfWayToCenter(prevTarget.X, prevTarget.Y) {
						continue
					}
					switch prevTarget.fusion.laneLandmark {
					case holo.LandmarkStraight:
						prevTarget.faker = newUniformTrajectoryFaker(prevTarget.Car_inf, fm.hardcodeFakeSpeed, fm.fps, set.RadarSection.ExitLaneAzimuth())
					case holo.LandmarkLeft:
						leftExit := set.LeftTurnEntrySection.ExitWayConfig.Data()
						xy := leftExit.RandomFakeDest()
						prevTarget.faker = newLeftRightTurnTrajectoryFaker3(fm.logger, prevTarget.Car_inf, fm.hardcodeFakeSpeed, -1.0, fm.fps, xy[0], xy[1], set.LeftTurnEntrySection.ExitLaneRadian())
					case holo.LandmarkRight:
						rightExit := set.RightTurnEntrySection.ExitWayConfig.Data()
						xy := rightExit.RandomFakeDest()
						prevTarget.faker = newLeftRightTurnTrajectoryFaker3(fm.logger, prevTarget.Car_inf, fm.hardcodeFakeSpeed, 1.0, fm.fps, xy[0], xy[1], set.RightTurnEntrySection.ExitLaneRadian())
					default:
						continue
					}
				}

				// 造假直到 x 或 y 超过阈值
				next, ok := prevTarget.faker.Next()
				if math.Abs(next.X) > fakeXThreshold || math.Abs(next.Y) > fakeYThreshold {
					continue
				}
				if !ok {
					if prevTarget.isFinalFaker {
						continue
					}

					switch prevTarget.fusion.laneLandmark {
					case holo.LandmarkLeft:
						prevTarget.faker = newUniformTrajectoryFaker(prevTarget.Car_inf, fm.hardcodeFakeSpeed, fm.fps, set.LeftTurnEntrySection.ExitLaneAzimuth())
					case holo.LandmarkRight:
						prevTarget.faker = newUniformTrajectoryFaker(prevTarget.Car_inf, fm.hardcodeFakeSpeed, fm.fps, set.RightTurnEntrySection.ExitLaneAzimuth())
					default:
						continue
					}

					prevTarget.isFinalFaker = true

					next, ok = prevTarget.faker.Next()
					if !ok {
						continue
					}
				}
				prevTarget.Car_inf = next
				set.Targets = append(set.Targets, *prevTarget)
			}
		}

		set.spliceByDirection(sortedSets, intersection, fm)
		if !fm.hideNonMotorVehicles {
			set.splice(sortedSets[i+1:], intersection, fm)
		}

		if fm.enableFakeHappyEyeballs {
			set.PrevTargets = set.PrevTargets[:0]
		}

		for i := range set.Targets {
			target := set.Targets[i] // explicit copy

			// 假目标无需 apply 直接跳过已拼接的目标
			if target.faker != nil && target.spliced ||
				target.faker == nil && !target.fusion.applyToTarget(fm.logger, &target.Car_inf) {
				continue
			}

			target.PutDebugText()
			splicedTargets = append(splicedTargets, target.Car_inf)
			set.processTargetEvent(&target.Car_inf)

			if fm.enableFakeHappyEyeballs {
				set.PrevTargets = append(set.PrevTargets, target)
			}

			if target.RawLaneID == 1 || target.RawLaneID == 255 || target.RawLaneID > maxMotorLaneID || target.RawY > set.EntrySection.EntryWayYStart {
				continue
			}
			target.Defuse()
			defusedTargets = append(defusedTargets, target.Car_inf)
		}

		set.emitEvents(fm.logger)

		// 雷达交通流和事件
		slices.SortFunc(defusedTargets, strcture.Car_inf.CompareByY)
		id := int(set.RadarSection.ID)
		radarEvent(id, defusedTargets)
		vd := video.VideoData{
			Frame_num:  set.FrameID,
			Video_data: defusedTargets,
		}
		var ok bool
		Per_second_data, ok = Per_second_data_all[id]
		if !ok {
			continue
		}
		lineFlow, _ := flow_calcu_muti(line_flow_all[id], line_cache[id], defusedTargets, id)
		vd.Video_flow = []map[string]int{lineFlow}
		video.AvgQueueDelay = set.AvgQueueDelay
		video.AvgStationaryDuration = set.AvgStationaryDuration
		set.AvgQueueDelay = 0
		set.AvgStationaryDuration = 0
		video.SignalCycle = time.Duration(intersection.SignalCycle) * time.Second
		video.Compute_flow(1, vd, Per_second_data, car_trajectory, id, 0)
		if set.FrameID%int(fm.fps) == 0 {
			// 过饱和计算
			for _, f := range lineFlow {
				set.FlowCount += f
			}
			set.FlowSec++
			if set.FlowSec >= intersection.SignalCycle {
				s := video.GetOrCreateFlowState(id)

				// 饱和度
				if set.EntrySection.FlowCapacity != 0 {
					s.Saturation = float64(set.FlowCount) / float64(set.EntrySection.FlowCapacity)
					if set.FlowCount > set.EntrySection.FlowCapacity {
						video.EmitRadarEvent(0, 1, "过饱和", strcture.Car_inf{
							DeviceID: set.RadarSection.RadarDeviceID,
						})
					}
				}

				// 停车次数
				if s.AvgLineup == 0 || set.EntrySection.GreenLightSeconds == 0 {
					// 随机 0.25 ~ 0.65
					s.ParkingTimes = 0.25 + 0.4*rand.Float64()
				} else {
					// 额外随机 0.55 ~ 1.25
					//
					// 逻辑引用苏秦原话：大概就是绿灯100秒，按3秒过一辆车计算，排队车头间距5.5m，
					// 100秒内能减少100/3*5.5m的排队长度，用当前排队长度除这个排队长度，就是
					// 末端到达的车辆大致要停止几次，但是也不完全准确，加了些随机数
					s.ParkingTimes = 0.55 + 0.7*rand.Float64() + s.AvgLineup/(float64(set.EntrySection.GreenLightSeconds)/3*5.5)
				}

				set.FlowSec = 0
				set.FlowCount = 0
			}

			// 过线计数清理
			line_flow_all[id] = map[string]int{}
			if set.FrameID%int(fm.fps*2*60) == 0 {
				line_cache[id] = map[int]bool{}
			}
		}
		set.FrameID++
	}

	return splicedTargets
}

func (fm *FusionManager) smoothTargets(targets []strcture.Car_inf) []strcture.Car_inf {
	if fm.disableTrajectorySmoothing {
		return targets
	}

	smoothedTargets := make([]strcture.Car_inf, 0, len(targets))
	for _, target := range targets {
		s := fm.targetSmootherCache.GetOrCreate(target.UID)
		xy, ok := s.Add(traffic.PointXY{X: target.X, Y: target.Y})
		if !ok {
			continue
		}
		target.X = xy.X
		target.Y = xy.Y
		smoothedTargets = append(smoothedTargets, target)
	}
	return smoothedTargets
}

func (fm *FusionManager) calculateAzimuth(targets []strcture.Car_inf) {
	for i := range targets {
		target := &targets[i]
		azimuthHint := target.Radian * math.Pi / 180 // TODO: 当前为了兼容前端，实际存的角度，以后应改为弧度
		azimuth, _ := fm.targetAzimuthCache.GetOrCreate(target.UID).Calculate(traffic.PointXY{X: target.X, Y: target.Y}, azimuthHint)
		target.Radian = azimuth * 180 / math.Pi
	}
}

const waitDuration = 20 * time.Millisecond

// Run 从注册的生产者读取轨迹数据，合并后输出到 outCh。
func (fm *FusionManager) Run(outCh chan<- []strcture.Car_inf) {
	for {
		fm.updateCachedPlates()
		sectionByDeviceID := holo.RoadSectionConfigByDeviceID()
		sectionByDirection := holo.RoadSectionConfigByDirection()
		setByDeviceID := make(map[string]*targetSpliceSet)
		var targetListCapacityHint int

		var intersection holo.IntersectionConfig
		intersection.Load()

		fm.mu.Lock()
		for producerCh, producerTargetSpliceSet := range fm.producerMap {
			select {
			case producerTargets := <-producerCh:
				if fm.hideNonMotorVehicles {
					producerTargets = removeNonMotorVehicles(producerTargets)
				}

				if fm.speedIsMps {
					fixSpeed(producerTargets)
				}

				if len(producerTargets) == 0 {
					producerTargetSpliceSet.Targets = nil
					continue
				}

				slices.SortFunc(producerTargets, strcture.Car_inf.CompareByID)

				deviceID := producerTargets[0].DeviceID
				radarSection, ok := sectionByDeviceID[deviceID]
				if !ok {
					fm.logger.Warn("Unknown device ID", zap.String("deviceID", deviceID))
					continue
				}
				entrySection := sectionByDirection[radarSection.Direction.Opposite()]

				producerTargetSpliceSet.RadarSection = radarSection
				producerTargetSpliceSet.EntrySection = entrySection
				producerTargetSpliceSet.LeftTurnEntrySection = sectionByDirection[entrySection.Direction.LeftTurn()]
				producerTargetSpliceSet.RightTurnEntrySection = sectionByDirection[entrySection.Direction.RightTurn()]

				// 如果有 UUID, 说明是新版录制，可以直接使用 UUID 分配连续 ID
				// 否则，需要使用 device ID + ID 模拟 UUID
				if producerTargets[0].UUID != "" {
					for i := range producerTargets {
						id := fm.u2id.Get(producerTargets[i].UUID)
						producerTargets[i].Id = int(id)
						producerTargets[i].UID = int(id)
					}
				} else {
					for i := range producerTargets {
						id := fm.u2id.Get(deviceID + strconv.Itoa(producerTargets[i].Id))
						producerTargets[i].Id = int(id)
						producerTargets[i].UID = int(id)
					}
				}

				// 过滤指定不要的目标
				if len(fm.excludedTargetIDs) > 0 {
					var filteredTargets []strcture.Car_inf
					for _, target := range producerTargets {
						if slices.ContainsFunc(fm.excludedTargetIDs, func(id int64) bool {
							return id == int64(target.Id)
						}) {
							continue
						}
						filteredTargets = append(filteredTargets, target)
					}
					producerTargets = filteredTargets
				}

				if fm.isRadar {
					producerTargets = applySectionFilter(radarSection, producerTargets)
				} else {
					convertToRadarLaneID(entrySection, producerTargets)
				}

				if fm.debugAddCalibrationPoints {
					calibration := entrySection.Calibration.Data()
					calPxPts := calibration.Video.Raw.PixelPoints

					for i := 0; i < len(calPxPts)/2; i++ {
						id := int(entrySection.ID)*1000000 + i
						px, py := calPxPts[i*2], calPxPts[i*2+1]
						producerTargets = append(producerTargets, strcture.Car_inf{
							Id:       id,
							UID:      id,
							Cls:      255,
							Speed:    255,
							DeviceID: deviceID + "-calibration",
							Coo_x:    px,
							Coo_y:    py,
						})
					}
				}

				if fm.isRadar {
					fm.convertRadarTargetCoordinates(radarSection, producerTargets)
				} else {
					fm.convertVideoTargetCoordinates(entrySection, producerTargets)
				}

				producerTargetSpliceSet.Targets = producerTargetSpliceSet.Targets[:0]
				for i := range producerTargets {
					producerTargetSpliceSet.Targets = append(producerTargetSpliceSet.Targets, targetInfo{
						Car_inf: producerTargets[i],
					})
				}

				if !fm.isRadar {
					producerTargetSpliceSet.calculateSpeed()
				}

			case <-time.After(waitDuration):
				if producerTargetSpliceSet.RadarSection == nil || len(producerTargetSpliceSet.Targets) == 0 {
					continue
				}
			}

			if fm.debugDisableFusion {
				continue
			}

			fm.buildTargetFusionInfoCache(&intersection, producerTargetSpliceSet)
			setByDeviceID[producerTargetSpliceSet.RadarSection.RadarDeviceID] = producerTargetSpliceSet
			targetListCapacityHint += len(producerTargetSpliceSet.Targets)
		}
		fm.mu.Unlock()

		if fm.debugDisableFusion {
			var targets []strcture.Car_inf
			for _, producerTargetSpliceSet := range fm.producerMap {
				for _, target := range producerTargetSpliceSet.Targets {
					target.PutDebugText()
					targets = append(targets, target.Car_inf)
				}
			}
			if len(targets) == 0 {
				time.Sleep(waitDuration)
				continue
			}
			outCh <- targets
		}

		if len(setByDeviceID) == 0 {
			time.Sleep(waitDuration)
			continue
		}

		sortedSets := make([]*targetSpliceSet, 0, len(setByDeviceID))
		for _, set := range setByDeviceID {
			sortedSets = append(sortedSets, set)
		}
		slices.SortFunc(sortedSets, func(s0, s1 *targetSpliceSet) int {
			return cmp.Compare(s0.RadarSection.ID, s1.RadarSection.ID)
		})

		targets := fm.spliceTargets(&intersection, sortedSets, targetListCapacityHint)
		targets = fm.smoothTargets(targets)
		fm.calculateAzimuth(targets)
		outCh <- targets
	}
}

func removeNonMotorVehicles(targets []strcture.Car_inf) []strcture.Car_inf {
	out := make([]strcture.Car_inf, 0, len(targets))
	for _, target := range targets {
		if target.IsMotorVehicle() {
			out = append(out, target)
		}
	}
	return out
}

func fixSpeed(targets []strcture.Car_inf) {
	for i := range targets {
		targets[i].Speed *= 3.6
	}
}

func applySectionFilter(section *holo.RoadSectionConfig, targets []strcture.Car_inf) []strcture.Car_inf {
	out := make([]strcture.Car_inf, 0, len(targets))
	for _, target := range targets {
		if target.Y < section.RadarMinRawY {
			continue
		}
		out = append(out, target)
	}
	return out
}

func convertToRadarLaneID(entrySection *holo.RoadSectionConfig, targets []strcture.Car_inf) {
	for i := range targets {
		targets[i].IdLane = int(radarLaneIDFromVideoLaneID(uint8(targets[i].IdLane), entrySection.EnterData.Data().LaneNumber))
	}
}
