package video

import (
	"encoding/json"
	"fmt"
	"strconv"
	"wukong/controller/defination"
	lib "wukong/lib"
	数据库对象 "wukong/model"
)

var VideoIdAuto int
var Max_frame_num int
var videoEndTime string
var AnalysisModel string

func VideoDetect(videoIdAuto, max_frame, idDefination int, endtime, analysisModel string) {
	VideoIdAuto = videoIdAuto
	videoEndTime = endtime
	AnalysisModel = analysisModel
	scheme_id_all = make(map[int]int)
	area_points_all = make(map[int]map[int][][]int)
	event_list_all = make(map[int]map[int]LaneData)
	Per_second_data_all = make(map[int]map[int]map[string][]float64)

	// edition := "交通分析版"
	if edition != "雷达版" {
		car_trajectory := map[int][][]float64{} //轨迹数据
		var l_frame string
		var l_frame_data map[int]VideoData
		frameNumAll := make(map[int]int)
		if edition == "交通分析版" {
			Stream_id = videoIdAuto
			scheme_id = idDefination
			// area_points, event_list, scheme_id = scheme_test(videoIdAuto)
			sql := fmt.Sprintf("select content from video_roi where  id=%d limit 1", idDefination)
			var roi_result scheme_roi
			rows := 数据库对象.QueryConfig(sql)
			for rows.Next() {
				err := rows.StructScan(&roi_result)
				area_points, _ = get_scheme_data(roi_result.Content)
				if err != nil {
					fmt.Println("scheme_test: ", err)
				}
			}
		} //记录各个流当前帧号，控制重复帧

		// lane:区域名称,初始化每秒交通流统计表
		// for _, values := range streamList {
		// 	Per_second_data_all[values.Id] = make(map[int]map[string][]float64)
		// 	for lane, _ := range area_points_all[values.Id] {
		// 		Per_second_data_all[values.Id][lane] = make(map[string][]float64)
		// 	}
		// }
		for lane, _ := range area_points {
			Per_second_data[lane] = make(map[string][]float64)
		}
		// fmt.Println("area: ", area_points)
		// fmt.Println("Per_second_data: ", Per_second_data)
		cur_frame := 0
		frame_interval := 299
		// next_frame := 0
		Max_frame_num = max_frame
		record_frame10 := 0
		record_frame60 := 0
		record_frame600 := 0
		record_frame3600 := 0
		var l_frame_list map[int]string
		// var l_frame_list_db []string
		var sql string
		for cur_frame <= Max_frame_num {
			l_frame_list = get_video_frames(cur_frame, cur_frame+frame_interval, scheme_id, Stream_id, float64(videoIdAuto))
			sql = "insert into scheme_data(frame_num, frame_data, id_defination, id_stream)"
			for i := cur_frame; i <= cur_frame+frame_interval; i++ {
				var frame string
				if _, ok := l_frame_list[i]; ok {
					frame = l_frame_list[i]
				} else {
					continue
				}
				// frame = logic.SmoothV2(logic.Fill(frame))
				l_frame = Func_目标速度(frame, 56, videoIdAuto) //帧数据
				sql = sql + " SELECT " + strconv.Itoa(i) + ", '" + l_frame + "', " + strconv.Itoa(scheme_id) + ", " + strconv.Itoa(videoIdAuto) + "  UNION ALL "
				json.Unmarshal([]byte(l_frame), &l_frame_data)
				// fmt.Println("-++++++++++++-: ", l_frame_data[videoIdAuto].Frame_num)
				for idStream, dataStream := range l_frame_data {
					if frameNumAll[idStream] != dataStream.Frame_num {
						frameNumAll[idStream] = dataStream.Frame_num
						Frame_num = dataStream.Frame_num
						if edition == "交通分析版" {
							Compute_flow(0, dataStream, Per_second_data, car_trajectory, videoIdAuto, scheme_id)
							if Frame_num%(DetectFPS*10) == 0 && Frame_num != Max_frame_num {
								统计流量(scheme_id, Frame_num, DetectFPS, 10, 0, len(Lane_points[videoIdAuto]))
								if Frame_num%(DetectFPS*60) == 0 {
									// fmt.Println("Frame_num: ", Frame_num)
									Save_flow(Frame_num, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
									record_frame60 = Frame_num
									if Frame_num%(DetectFPS*600) == 0 {
										record_frame600 = dataStream.Frame_num
										if Frame_num%(DetectFPS*3600) == 0 {
											record_frame3600 = Frame_num
										}
									}
								}
								record_frame10 = Frame_num
							} else if Frame_num == Max_frame_num {
								if lib.Frame2Time(Max_frame_num, DetectFPS) != lib.Frame2Time(record_frame10, DetectFPS) {
									统计流量(scheme_id, record_frame10+DetectFPS*10, DetectFPS, 10, 0, len(Lane_points[videoIdAuto]))
								}
								if Frame_num%(DetectFPS*3600) != 0 {
									// 统计流量(scheme_id, record_frame3600+DetectFPS*3600, DetectFPS, 10, 0, len(Lane_points[videoIdAuto]))
									if Frame_num%(DetectFPS*600) != 0 {
										if Frame_num%(DetectFPS*60) != 0 {
											定时计算流量(record_frame60+DetectFPS*60, DetectFPS, 60, 10, 0)
											Save_flow(record_frame60+DetectFPS*60, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
										} else {
											定时计算流量(Frame_num, DetectFPS, 60, 10, 0)
											Save_flow(Frame_num, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
										}
										if (record_frame60+DetectFPS*60)%600 != 0 {
											定时计算流量(record_frame600+DetectFPS*600, DetectFPS, 600, 60, 0)
											统计区域信息(record_frame600+DetectFPS*600, DetectFPS, 600, 60, 0, len(Per_second_data))
										}
									} else {
										定时计算流量(Frame_num, DetectFPS, 600, 60, 0)
										Save_flow(Frame_num, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
									}
									if (record_frame60+DetectFPS*60)%3600 != 0 {
										定时计算流量(record_frame3600+DetectFPS*3600, DetectFPS, 3600, 600, 0)
										统计区域信息(record_frame3600+DetectFPS*3600, DetectFPS, 3600, 600, 0, len(Per_second_data))
									}
								} else {
									Save_flow(Frame_num, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
									定时计算流量(Frame_num, DetectFPS, 3600, 600, 0)
								}
								// 统计流量(scheme_id, record_frame60+DetectFPS*60, DetectFPS, 10, 0, len(Lane_points[videoIdAuto]))
								// 统计流量(scheme_id, record_frame600+DetectFPS*600, DetectFPS, 10, 0, len(Lane_points[videoIdAuto]))

								// Save_flow(record_frame60+DetectFPS*60, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
							}
						} else {
							Compute_flow(0, dataStream, Per_second_data, car_trajectory, idStream, scheme_id)
						}
					}
				}
				// time.Sleep(1 * time.Millisecond)
			}

			sql = sql[:len(sql)-11]
			// fmt.Println(sql)
			数据库对象.ExecFrames(sql, float64(videoIdAuto))

			cur_frame = cur_frame + frame_interval

		}
		数据库对象.ExecConfig(fmt.Sprintf("update video_roi set is_calculating=0 where id=%d", scheme_id))
		defination.Is_video_detect = 0
	}
}
