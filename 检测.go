package video

import (
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"strconv"
	"sync"
	"time"

	//"math"

	"wukong/controller/global"
	setting "wukong/controller/setting"
	"wukong/handler/middleWare"
	lib "wukong/lib"
	logic "wukong/logic/video"
	数据库对象 "wukong/model"
	"wukong/model/config"
	平台协议 "wukong/protocol/platform/northbound"
	"wukong/strcture"

	"github.com/gorilla/websocket"
)

var (
	DetectRange [][]float64 // 新视能 检测范围
	DeviceUId   string      //新视能 设备编号
)

// 视频信息
type VideoData struct {
	Frame_num  int                         `json:"frame_num"`
	Video_data []strcture.Car_inf          `json:"video_data"`
	Frame_inf  []int                       `json:"frame_inf"`
	Video_flow []map[string]int            `json:"video_flow"`
	Class_flow []map[string]map[string]int `json:"class_flow"`
	Roi_data   map[string][]float64        `json:"roi_data"`
}

// 轨迹数据
type trajectory struct {
	frame_num int
	coor_list [][]float64
}

// 事件存储
type event_data struct {
	Id                 int    `json:"id"`
	StreamId           int    `json:"streamId"` //传给deep的是video_administer表里的streamId
	Type               string `json:"type"`
	Bbox               []int  `json:"bbox"`
	IdAuto             int64  `json:"idAuto"`
	VideoPath          string `json:"videoPath"`
	PathSnapshot       string `json:"pathSnapshot"`
	PathSnapshotTarget string `json:"pathSnapshotTarget"`
}

var clsMapStr = map[string]string{"person": "行人", "motorcycle": "非机动车", "car": "小客车", "bus": "大客车", "truck": "货车"}
var clsMapNum = map[string]string{"0": "行人", "1": "非机动车", "2": "小客车", "3": "非机动车", "5": "大客车", "6": "货车", "7": "货车"}

var event_save []event_data
var event_to_deep []event_data
var event_flag int = 0

// lock
var DetectLock sync.RWMutex

// var cars_stop map[int]int

// 求和
func Sum(data []float64) float64 {
	sum := 0.0
	for _, v := range data {
		sum = sum + v
	}
	return sum
}

type StreamIdStruct struct {
	StreamId int `db:"stream_id"`
}

// 计算平均速度
func Get_speed_avg(data []strcture.Car_inf) float64 {
	avg := 0.0
	sum := 0.0
	for _, value := range data {
		sum = sum + value.Speed
	}
	if len(data) != 0 {
		avg = sum / float64(len(data))
	}
	return avg
}

// 平均数
func Get_avg(data []float64) float64 {
	avg := 0.0
	sum := 0.0
	num := 0.0
	for _, value := range data {
		sum = sum + value
		if value != 0 {
			num = num + 1
		}
	}
	if num != 0 {
		avg = sum / num
	}
	return avg
}

// 计算间距
func Get_dis_avg(data []strcture.Car_inf) float64 {
	avg := 0.0
	dis_sum := 0.0
	dis_num := 0.0
	for i := 0; i+1 < len(data); i++ {
		if data[i+1].Y-data[i].Y < 100 {
			dis_sum = dis_sum + data[i+1].Y - data[i].Y
			dis_num = dis_num + 1
		}
	}
	if dis_num != 0 {
		avg = dis_sum / dis_num
	}
	return avg
}

// 排队长度
func Sort_compute(cars []strcture.Car_inf) float64 {
	sort := 0.0
	if len(cars) >= 2 {
		for i := 0; i+1 < len(cars); i++ {
			if cars[i+1].Y-cars[i].Y > 15 {
				sort = cars[i].Y - cars[0].Y + 4
				break
			} else if i == len(cars)-2 {
				sort = cars[i+1].Y - cars[0].Y
			}
		}
	}
	return sort
}

// 分离每车道车辆数据
func get_lane_cars(l_obj_data []strcture.Car_inf, area_points map[int][][]int) map[int][]strcture.Car_inf {
	lane_cars := make(map[int][]strcture.Car_inf)
	for i := 0; i < len(l_obj_data); i++ {
		//fmt.Println("lane: ", l_obj_data[i].Coo_x, l_obj_data[i].Coo_y)
		point := []int{int(l_obj_data[i].Coo_x), int(l_obj_data[i].Coo_y)}
		for lane, pts := range area_points {
			if ray(point, pts) == 1 {
				lane_cars[lane] = append(lane_cars[lane], l_obj_data[i])
				//break
			}
		}
	}
	return lane_cars
}

// 分离雷达每车道车辆数据
func get_radar_lane_cars(l_obj_data []strcture.Car_inf) map[int][]strcture.Car_inf {
	lane_cars := make(map[int][]strcture.Car_inf)
	for i := 0; i < len(l_obj_data); i++ {
		lane := l_obj_data[i].IdLane
		if lane > 0 {
			lane_cars[lane] = append(lane_cars[lane], l_obj_data[i])
		}
	}
	return lane_cars
}

// 计算iou
func computeIOU(gtBox, bBox []int) float64 {
	width0 := float64(gtBox[2]) - float64(gtBox[0])
	height0 := float64(gtBox[3]) - float64(gtBox[1])
	width1 := float64(bBox[2]) - float64(bBox[0])
	height1 := float64(bBox[3]) - float64(bBox[1])

	maxX := math.Min(float64(gtBox[2]), float64(bBox[2]))
	minX := math.Max(float64(gtBox[0]), float64(bBox[0]))
	width := maxX - minX

	maxY := math.Min(float64(gtBox[3]), float64(bBox[3]))
	minY := math.Max(float64(gtBox[1]), float64(bBox[1]))
	height := maxY - minY

	// fmt.Println("xiaoyu1: ", gtBox[3], bBox[3], maxY, minY)

	if width <= 0 || height <= 0 {
		// fmt.Println("xiaoyu: ", width, height)
		return 0.0
	}

	interArea := width * height
	boxAArea := width0 * height0
	boxBArea := width1 * height1

	if boxAArea+boxBArea-interArea == 0 {
		return 1.0
	}

	// iou := interArea / (boxAArea + boxBArea - interArea)
	// fmt.Println("boxAArea: ", interArea, boxAArea)
	iou := interArea / boxAArea

	return iou
}

type StructCurveData struct {
	Angle     float64         `json:"angle"`
	B         float64         `json:"b"`
	K         float64         `json:"k"`
	R         float64         `json:"r"`
	LaneLineX map[int]float64 `json:"laneLineX"`
}

// 计算车道号(下半部分直道及中间部分弯道)
func ComputeLaneNumber(x, y float64) int {
	var CurveDataFormat StructCurveData
	json.Unmarshal([]byte(middleWare.CurveData), &CurveDataFormat)
	theta := CurveDataFormat.Angle
	k := CurveDataFormat.K
	r := CurveDataFormat.R
	b := CurveDataFormat.B
	laneInfo := CurveDataFormat.LaneLineX
	// 雷达左正右负取相反数
	x = -x
	cutOffPoint := getY(r, b, k)        //下切点
	cutUpPointX := getX(r, math.Abs(k)) //上切点
	cutUpPointY := 1000.0
	if cutUpPointX > 0 {
		cutUpPointY = math.Sqrt(r*r-(cutUpPointX-r)*(cutUpPointX-r)) + b - (r * math.Tan(theta))
	}
	//中心点x坐标
	x_lane := 0.0
	if k == 0 {
		cutOffPoint = 9999
		cutUpPointY = 9999
	}
	//判断在是否在弯道区域
	if y >= cutOffPoint && y < cutUpPointY {
		x_lane = r - math.Sqrt(r*r-math.Pow((y+r*math.Tan(theta)-b), 2))
		if k < 0 {
			x_lane = -x_lane
		}
	} else if y >= cutUpPointY {
		x_lane = (y - b) / k
	}
	//判断车在车道内
	for i := 1; i <= len(laneInfo); i++ {
		if x >= x_lane+laneInfo[i] && x < x_lane+laneInfo[i+1] {
			return i
		}
	}
	return 0
}

// 计算下切点坐标
func getY(r, b, k float64) float64 {
	return b - r*math.Tan(math.Pi/4-math.Atan(math.Abs(k))/2)
}

// 计算上切点坐标
func getX(r, k float64) float64 {
	xita := math.Tan(math.Pi/4 - math.Atan(k)/2)
	x := math.Sqrt((xita*k-1)*(xita*k-1)*r*r/((k*k+1)*(k*k+1))-(r*r*xita*xita)/(k*k+1)) - (r*xita*k-r)/(k*k+1)
	return x
}

// 排序
type sort_car []strcture.Car_inf

func (s sort_car) Len() int           { return len(s) }
func (s sort_car) Swap(i, j int)      { s[i], s[j] = s[j], s[i] }
func (s sort_car) Less(i, j int) bool { return s[i].Y < s[j].Y }

var lane_cars map[int][]strcture.Car_inf

var event_list map[int]LaneData
var area_points map[int][][]int
var scheme_id int

var event_list_all map[int]map[int]LaneData
var area_points_all map[int]map[int][][]int
var scheme_id_all map[int]int

var radarFrameNum = 0

var Per_second_data = make(map[int]map[string][]float64)     //每秒的交通流数据
var Frame_num int                                            // 帧数
var DetectFPS int                                            // 帧率
var Stream_id int                                            // 交通分析版，单路版，视频流ID
var Per_second_data_all map[int]map[int]map[string][]float64 //多路结构的每秒的交通流数据
var Id_cls = map[int]map[int]string{0: map[int]string{}, 1: map[int]string{}}
var person_in_car = make(map[int]int) //视频检测筛选车里的人

func Compute_flow(deviceType int, l_frame_data VideoData,
	Per_second_data map[int]map[string][]float64, car_trajectory map[int][][]float64, idStream, scheme_id int) {
	if global.Edition != "交通分析版" {
		if deviceType == 0 {
			DetectFPS = 22
		} else {
			DetectFPS = 14
		}
	}

	l_obj_data := l_frame_data.Video_data
	// d1, _ := json.Marshal(l_obj_data)
	// d2, _ := json.Marshal(l_frame_data)
	// fmt.Println("data1: ", string(d1))
	// fmt.Println("data2: ", string(d2))

	if deviceType == 1 {
		radarFrameNum = l_frame_data.Frame_num
	}
	// 轨迹存储
	if l_frame_data.Frame_num%(DetectFPS/5) == 0 {
		for i := 0; i < len(l_obj_data); i++ {
			now := float64(time.Now().Unix())
			var trj []float64
			if deviceType == 1 {
				trj = []float64{l_obj_data[i].X, l_obj_data[i].Y, l_obj_data[i].Speed, now, float64(l_frame_data.Frame_num), l_obj_data[i].X, l_obj_data[i].Y}
			} else {
				trj = []float64{l_obj_data[i].X, l_obj_data[i].Y, l_obj_data[i].Speed, now, float64(l_frame_data.Frame_num), l_obj_data[i].Coo_x, l_obj_data[i].Coo_y}
			}
			car_trajectory[l_obj_data[i].Id] = append(car_trajectory[l_obj_data[i].Id], trj)
			Id_cls[deviceType][l_obj_data[i].Id] = strconv.Itoa(l_obj_data[i].Cls)
		}
	}
	if edition == "交通分析版" {
		if l_frame_data.Frame_num%((DetectFPS/2)*4) == 0 && l_frame_data.Frame_num != Max_frame_num {
			//fmt.Println(car_trajectory)
			for id, trj := range car_trajectory {
				if (l_frame_data.Frame_num - int(trj[len(trj)-1][4])) > DetectFPS*2 {
					if len(trj) > 5 {
						if AnalysisModel == "路段视角" {
							trj = Func_车辆轨迹优化(trj, idStream, 0)
						} else {
							// trj = MovingAverage2(trj, 5)
							trj = Func_车辆轨迹优化(trj, idStream, 0.3)
						}
						// trj = MovingAverage2(trj, 20)
						if len(trj) > 0 {
							sql_trj, _ := json.Marshal(trj)
							// fmt.Println("trj:", string(sql_trj))
							trj_time := lib.Frame2Time(int(trj[len(trj)-1][4]), DetectFPS)
							// 数据库对象.ExecTraffic(fmt.Sprintf("update traffic_target set target_trajectory='%s' where id_auto = (select id_auto from traffic_target where id_stream=%d and id_defination=%d and target_id=%d and device_type=%d Order By created_at DESC Limit 1) ", string(sql_trj), idStream, scheme_id, id, deviceType))
							数据库对象.ExecTraffic(fmt.Sprintf(`insert into traffic_target_trajectory (id_gate, device_type,id_defination,target_id,
							target_type,created_at,id_stream,target_trajectory) 
							values('%d','%s','%d','%d','%s','%s', '%d','%s')`,
								0, "0", scheme_id, id, clsMapStr[Id_cls[deviceType][id]], trj_time, Stream_id, string(sql_trj)))
						}
					}
					delete(car_trajectory, id)
					delete(Id_cls[deviceType], id)
					continue
				}
			}
		} else if l_frame_data.Frame_num == Max_frame_num {
			for id, trj := range car_trajectory {
				if (l_frame_data.Frame_num - int(trj[len(trj)-1][4])) < DetectFPS*2 {
					if len(trj) > 5 {
						if AnalysisModel == "路段视角" {
							trj = Func_车辆轨迹优化(trj, idStream, 0)
						} else {
							// trj = MovingAverage2(trj, 5)
							trj = Func_车辆轨迹优化(trj, idStream, 0.3)
						}
						// trj = MovingAverage2(trj, 20)
						if len(trj) > 0 {
							sql_trj, _ := json.Marshal(trj)
							// fmt.Println("trj:", string(sql_trj))
							trj_time := lib.Frame2Time(int(trj[len(trj)-1][4]), DetectFPS)
							// 数据库对象.ExecTraffic(fmt.Sprintf("update traffic_target set target_trajectory='%s' where id_auto = (select id_auto from traffic_target where id_stream=%d and id_defination=%d and target_id=%d and device_type=%d Order By created_at DESC Limit 1) ", string(sql_trj), idStream, scheme_id, id, deviceType))
							数据库对象.ExecTraffic(fmt.Sprintf(`insert into traffic_target_trajectory (id_gate, device_type,id_defination,target_id,
							target_type,created_at,id_stream,target_trajectory) 
							values('%d','%s','%d','%d','%s','%s', '%d','%s')`,
								0, "0", scheme_id, id, clsMapStr[Id_cls[deviceType][id]], trj_time, Stream_id, string(sql_trj)))
						}
					}
					delete(car_trajectory, id)
					delete(Id_cls[deviceType], id)
					continue
				}
			}
		}
	} else if deviceType == 0 {
		if l_frame_data.Frame_num%((DetectFPS/2)*4) == 0 {
			//fmt.Println(car_trajectory)
			for id, trj := range car_trajectory {
				if (l_frame_data.Frame_num - int(trj[len(trj)-1][4])) > DetectFPS*2 {
					if len(trj) > 5 {
						// trj = Func_车辆轨迹优化(trj, idStream, 0)
						trj = MovingAverage2(trj, 5)
						if len(trj) > 0 {
							// fmt.Println(id, Id_cls, clsMapNum)
							sql_trj, _ := json.Marshal(trj)
							数据库对象.ExecTraffic(fmt.Sprintf(`insert into traffic_target_trajectory (id_gate, device_type,id_defination,target_id,
							target_type,created_at,id_stream,target_trajectory) 
							values('%d','%s','%d','%d','%s','%s', '%d','%s')`,
								0, "0", scheme_id, id, clsMapNum[Id_cls[deviceType][id]], time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"), Stream_id, string(sql_trj)))
						}
					}
					delete(car_trajectory, id)
					delete(Id_cls[deviceType], id)
					continue
				}
			}
		}
	}

	//fmt.Println("data11: ", l_obj_data)
	//fmt.Println("data22: ", l_frame_data)
	lane_cars := make(map[int][]strcture.Car_inf)
	if deviceType == 0 {
		lane_cars = get_lane_cars(l_obj_data, area_points) //每车道车辆数据
		// fmt.Println("lane: ", l_obj_data, area_points)
		// fmt.Println("lane2: ", lane_cars)
	} else {
		lane_cars = get_radar_lane_cars(l_obj_data) //每车道车辆数据
	}

	//fmt.Println("compute: ", lane_cars, area_points)
	// 新视能 存储各个车道交通流数据
	allFlowData := []map[string]interface{}{}
	//var lane_len float64 = 100 //车道长度（测试）
	for lane, cars := range lane_cars {
		sort.Sort(sort_car(cars))
		if global.Edition == "巡检版" {
			if global.IsStartPatrol == 0 {
				if l_frame_data.Frame_num%60 == 0 {
					// fmt.Println("ccccccccccccccccccccccccccccccccccc: ", cars)
					record_id, gps, _ := Func_巡查轨迹记录(0)
					for i := 0; i < len(cars); i++ {
						DetectCracks(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 4, 30, record_id, gps)
					}
				}
			} else {
				Func_巡查轨迹记录(1)
			}

		} else if deviceType == 0 && time.Now().Hour() < 19 && time.Now().Hour() > 6 {
			for i := 0; i < len(cars); i++ {
				if event_list_all[idStream][lane].Event.overspeed == 1 {
					DetectOverspeed(l_frame_data.Frame_num, cars[i], event_list_all[idStream][lane].Event.max_speed, lane, idStream, deviceType, 3, DetectFPS)
				}
				if event_list_all[idStream][lane].Event.lowspeed == 1 {
					DetectLowspeed(l_frame_data.Frame_num, cars[i], event_list_all[idStream][lane].Event.min_speed, lane, idStream, deviceType, 5, DetectFPS)
				}
				if event_list_all[idStream][lane].Event.stop == 1 {
					DetectStop(l_frame_data.Frame_num, cars[i], event_list_all[idStream][lane].Event.stop_time, lane, idStream, deviceType, DetectFPS)
				}
				if event_list_all[idStream][lane].Event.motor == 1 {
					// DetectMotor(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 2, DetectFPS)
					DetectBreakInto(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 2, DetectFPS, "非机动车闯入", "noCar")
				}
				if event_list_all[idStream][lane].Event.people == 1 {
					if global.Edition == "服务器版" {
						DetectBreakInto(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 2, DetectFPS, "行人闯入", "person")
					} else {
						notPerson := 0
						for p := 0; p < len(cars); p++ {
							if p != i && cars[p].Cls != 0 && cars[i].Cls == 0 {
								if computeIOU(cars[i].Bbox, cars[p].Bbox) > 0.3 {
									notPerson = 1
								}
								// fmt.Println("iou: ", cars[i].Bbox, cars[p].Bbox, computeIOU(cars[i].Bbox, cars[p].Bbox))
							}
						}
						if notPerson == 0 {
							DetectPeople(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 5, DetectFPS)
						}
					}
				}
				if event_list_all[idStream][lane].Event.traffic_cone == 1 {
					// DetectPeople(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 4, DetectFPS)
					DetectBreakInto(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 4, DetectFPS, "道路施工", "traffic_cone")
				}
				if event_list_all[idStream][lane].Event.back == 1 {
					DetectBack(l_frame_data.Frame_num, cars[i], event_list_all[idStream][lane], lane, idStream, deviceType, 1, DetectFPS)
				}
				if event_list_all[idStream][lane].Event.laneChange == 1 {
					DetectLaneChange(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 2, DetectFPS)
				}
				if event_list_all[idStream][lane].Event.hit_guardrail == 1 {
					DetectBreakInto(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 1, DetectFPS, "护栏防撞", "car")
				}
				if event_list_all[idStream][lane].Event.emergency == 1 {
					DetectBreakInto(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 1, DetectFPS, "机动车闯入", "car")
				}
				if event_list_all[idStream][lane].Event.vehicle_inward == 1 {
					DetectEmergency(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 1, DetectFPS, "车辆汇入")
				}
				if event_list_all[idStream][lane].Event.pedestrian_crossing == 1 {
					fmt.Println("行人穿越触发检测")
					DetectEmergency(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 1, DetectFPS, "行人穿越")
				}
				if event_list_all[idStream][lane].Event.tunnel == 1 {
					DetectEmergency(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 1, DetectFPS, "隧道提醒")
				}
				if event_list_all[idStream][lane].Event.accident == 1 {
					DetectAccident(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 1, DetectFPS)
				}
			}
		}
		if _, ok := Per_second_data[lane]; ok && l_frame_data.Frame_num%DetectFPS == 0 {
			Per_second_data[lane]["speed"] = append(Per_second_data[lane]["speed"], Get_speed_avg(cars))
			Per_second_data[lane]["head_dis"] = append(Per_second_data[lane]["head_dis"], Get_dis_avg(cars))
			var per_density float64
			if deviceType == 1 && middleWare.LaneLength > 0 {
				per_density = float64(len(cars)) * 1000 / middleWare.LaneLength
			} else {
				if global.Edition != "交通分析版" {
					if Num_len[idStream] != 1 {
						Calculate_roi_length(idStream, scheme_id)
					}
					len_y = Roi_data_go[idStream]
				} else {
					if Num_len[VideoIdAuto] != 1 {
						Calculate_roi_length(VideoIdAuto, scheme_id)
					}
					len_y = Roi_data_go[VideoIdAuto]
				}
				if len_y[lane] != 0 {
					per_density = float64(len(cars)) * 1000 / (len_y[lane])
				} else {
					per_density = 0
				}
				// fmt.Println(scheme_id, "检测： ", per_density, len(cars), len_y, lane, len_y[lane])
			}
			Per_second_data[lane]["density"] = append(Per_second_data[lane]["density"], per_density)
			Per_second_data[lane]["lineup"] = append(Per_second_data[lane]["lineup"], Sort_compute(cars))
		}

		//fmt.Println("``````````````````````````: ", l_frame_data.Frame_num)
		if _, ok := Per_second_data[lane]; ok && l_frame_data.Frame_num%(DetectFPS*30) == 0 {
			density := Get_avg(Per_second_data[lane]["density"])
			avg_speed := Get_avg(Per_second_data[lane]["speed"])
			avg_head_dis := Get_avg(Per_second_data[lane]["head_dis"])
			var lineup float64
			if len(Per_second_data[lane]["lineup"]) > 0 {
				lineup = Per_second_data[lane]["lineup"][len(Per_second_data[lane]["lineup"])-1]
			} else {
				lineup = 0
			}

			var avg_head_time float64
			if avg_speed == 0 {
				avg_head_time = avg_head_dis / avg_speed
			} else {
				avg_head_time = 0
			}

			space_occupancy := density * 5 / 10
			time_occupancy := density * 6.5 / 10
			//fmt.Println("flow:  ", lane, density, avg_head_dis, avg_head_time, space_occupancy, time_occupancy)

			check, ok := setting.EventThresholdMap[lane]
			if !ok {
				continue
			}

			if deviceType == 0 && event_list[lane].Event.jam == 1 && lineup > float64(event_list[lane].Event.cars_lenth) {
				add_time, _ := time.ParseDuration("30s")
				// sqlStr := fmt.Sprintf("insert into traffic_event(id_target,event_type,level,id_lane,event_start_time,event_end_time,created_at,is_send,id_zone,path_snapshot,path_video,id_stream,path_snapshot_target,device_type) values(%d,'%s',%d,%d,'%s','%s','%s',%d,%d,'%s','%s',%d,'%s',%d)",
				// 	0, "车道拥堵", 3, lane,
				// 	time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
				// 	time.Unix(time.Now().Add(add_time).Unix(), 0).Format("2006-01-02 15:04:05"),
				// 	time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
				// 	0, scheme_id, " ", " ", idStream, " ", deviceType)
				// //fmt.Printf(sqlStr)
				// 数据库对象.ExecTraffic(sqlStr)

				jam_bbox := []int{0, 0, 1920, 1080}
				jam_time := time.Unix(time.Now().Add(add_time).Unix(), 0).Format("2006-01-02 15:04:05")
				jam_gps := GPS
				update_event_db(jam_bbox, 0, "车道拥堵", scheme_id, lane, 1, 0, jam_time, idStream, 2, jam_gps, deviceType)

			} else if deviceType == 1 && check.Setting["拥堵事件"]["检测状态"] == 1 {
				filterData := filterObjectData(l_obj_data)
				for idLane, laneCars := range filterData {
					laneAvgSpeed := laneCars["speed"] / laneCars["num"]
					if laneAvgSpeed < float64(check.Setting["拥堵事件"]["速度阈值"]) && int(laneCars["num"]) > check.Setting["拥堵事件"]["目标数量"] {
						add_time, _ := time.ParseDuration("10s")
						sqlStr := fmt.Sprintf("insert into traffic_event(id_target,event_type,level,id_lane,event_start_time,event_end_time,created_at,is_send,id_zone,path_snapshot,path_video,id_stream,path_snapshot_target,device_type) values(%d,'%s',%d,%d,'%s','%s','%s',%d,%d,'%s','%s',%d,'%s',%d)",
							0, "车道拥堵", 3, idLane,
							time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
							time.Unix(time.Now().Add(add_time).Unix(), 0).Format("2006-01-02 15:04:05"),
							time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
							0, scheme_id, " ", " ", idStream, " ", deviceType)
						//fmt.Printf(sqlStr)
						数据库对象.ExecTraffic(sqlStr)
						// 新视能 上传拥堵事件数据
						if 平台协议.IsSendXSN {
							eventInfo := map[string]interface{}{
								"id":        lane,
								"idLane":    lane,
								"type":      3,
								"position":  []float64{},
								"starttime": time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05"),
								"plate":     "",
								"camera":    "",
								"parameters": map[string]float64{
									"avgSpeed":  avg_speed,
									"spaceRate": space_occupancy,
								},
							}
							平台协议.SendEventData(eventInfo)
						}
					}
				}

			}
			sub_time, _ := time.ParseDuration("-30s")
			starttime := time.Unix(time.Now().Add(sub_time).Unix(), 0).Format("2006-01-02 15:04:05")
			endtime := time.Unix(time.Now().Unix(), 0).Format("2006-01-02 15:04:05")

			timeRange := []string{starttime, endtime}
			laneFlowData := map[string]interface{}{
				"carDis":          avg_head_dis,
				"carTime":         avg_head_time,
				"density":         density,
				"idLane":          lane,
				"spaceRate":       space_occupancy,
				"speed":           avg_speed,
				"timeRate":        time_occupancy,
				"detectRange":     DetectRange,
				"count":           len(cars),
				"deviceNumber":    DeviceUId,
				"timeGranularity": 30,
				"timeRange":       timeRange,
			}
			allFlowData = append(allFlowData, laneFlowData)
		}
	}
	if 平台协议.IsSendXSN {
		平台协议.SendTrafficFlowData(allFlowData)
	}
	for lane, _ := range lane_cars {
		lane_cars[lane] = lane_cars[lane][0:0]
	}
}

// 占道经营事件
func Compute_roadside_booths(deviceType int, l_frame_data VideoData,
	Per_second_data map[int]map[string][]float64, car_trajectory map[int][][]float64, idStream int) {

	DetectFPS := 0
	if deviceType == 0 {
		DetectFPS = 25
	} else {
		DetectFPS = 14
	}
	// roi_data := l_frame_data.Roi_data
	l_obj_data := l_frame_data.Video_data

	if deviceType == 1 {
		radarFrameNum = l_frame_data.Frame_num
	}
	if deviceType == 0 {
		lane_cars = get_lane_cars(l_obj_data, area_points) //每车道车辆数据
	} else {
		lane_cars = get_radar_lane_cars(l_obj_data) //每车道车辆数据
	}

	//var lane_len float64 = 100 //车道长度（测试）
	for lane, cars := range lane_cars {
		sort.Sort(sort_car(cars))
		if deviceType == 0 {
			for i := 0; i < len(cars); i++ {
				DetectRoadsideBooths(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 2, DetectFPS)
			}
		}
	}
}

// 未戴口罩事件
func Compute_mask(deviceType int, l_frame_data VideoData,
	Per_second_data map[int]map[string][]float64, car_trajectory map[int][][]float64, idStream int) {
	DetectFPS := 0
	if deviceType == 0 {
		DetectFPS = 25
	} else {
		DetectFPS = 14
	}
	l_obj_data := l_frame_data.Video_data

	if deviceType == 1 {
		radarFrameNum = l_frame_data.Frame_num
	}

	lane_cars := make(map[int][]strcture.Car_inf)
	if deviceType == 0 {
		lane_cars = get_lane_cars(l_obj_data, area_points) //每车道车辆数据
	} else {
		lane_cars = get_radar_lane_cars(l_obj_data) //每车道车辆数据
	}

	//var lane_len float64 = 100 //车道长度（测试）
	for lane, cars := range lane_cars {
		sort.Sort(sort_car(cars))
		if deviceType == 0 {
			for i := 0; i < len(cars); i++ {
				DetectMask(l_frame_data.Frame_num, cars[i], lane, idStream, deviceType, 2, DetectFPS)
			}
		}
	}
}

// 过滤掉行人及非机动车数据
func filterObjectData(objs []strcture.Car_inf) map[int]map[string]float64 {
	carsFiltedData := make(map[int]map[string]float64)
	for _, obj := range objs {
		if obj.Kind != 3 && obj.Kind != 4 && obj.Kind != 5 {
			if _, ok := carsFiltedData[obj.IdLane]; !ok {
				carsFiltedData[obj.IdLane] = make(map[string]float64)
			} else {
				carsFiltedData[obj.IdLane]["speed"] += obj.Speed
				carsFiltedData[obj.IdLane]["num"] += 1.0
			}
		}
	}
	return carsFiltedData
}

func VideoDetectInit() []config.StructStream {
	scheme_id_all = make(map[int]int)
	area_points_all = make(map[int]map[int][][]int)
	event_list_all = make(map[int]map[int]LaneData)
	Per_second_data_all = make(map[int]map[int]map[string][]float64)
	streamList := config.GetStreamList()
	for _, values := range streamList {
		if values.IsDetected == 1 {
			cars_lowspeed[0][values.Id] = make(map[int]int)
			cars_lowspeed[1][values.Id] = make(map[int]int)
			cars_overspeed[0][values.Id] = make(map[int]int)
			cars_overspeed[1][values.Id] = make(map[int]int)
			cars_people[0][values.Id] = make(map[int]int)
			cars_people[1][values.Id] = make(map[int]int)
			cars_motor[0][values.Id] = make(map[int]int)
			cars_motor[1][values.Id] = make(map[int]int)
			cars_stop[0][values.Id] = make(map[int]int)
			cars_stop[1][values.Id] = make(map[int]int)
			cars_dregs[0][values.Id] = make(map[int]int)
			cars_dregs[1][values.Id] = make(map[int]int)
			cars_mask[0][values.Id] = make(map[int]int)
			cars_mask[1][values.Id] = make(map[int]int)
			cars_safety_helmet[0][values.Id] = make(map[int]int)
			cars_safety_helmet[1][values.Id] = make(map[int]int)
			cars_roadside_booths[0][values.Id] = make(map[int]int)
			cars_roadside_booths[1][values.Id] = make(map[int]int)
			cars_emergency[0][values.Id] = make(map[int]int)
			cars_emergency[1][values.Id] = make(map[int]int)
			carsLaneChange[0][values.Id] = make(map[int]map[string]int)
			carsLaneChange[1][values.Id] = make(map[int]map[string]int)
			carsStopToRun[0][values.Id] = make(map[int]map[string]int)
			carsStopToRun[1][values.Id] = make(map[int]map[string]int)
			carsRunToStop[0][values.Id] = make(map[int]map[string]float64)
			carsRunToStop[1][values.Id] = make(map[int]map[string]float64)
			carsBack[0][values.Id] = make(map[int]map[string]float64)
			carsBack[1][values.Id] = make(map[int]map[string]float64)

			cars_lowspeed_time[0][values.Id] = make(map[int]string)
			cars_lowspeed_time[1][values.Id] = make(map[int]string)
			cars_overspeed_time[0][values.Id] = make(map[int]string)
			cars_overspeed_time[1][values.Id] = make(map[int]string)
			cars_people_time[0][values.Id] = make(map[int]string)
			cars_people_time[1][values.Id] = make(map[int]string)
			cars_motor_time[0][values.Id] = make(map[int]string)
			cars_motor_time[1][values.Id] = make(map[int]string)
			cars_stop_time[0][values.Id] = make(map[int]string)
			cars_stop_time[1][values.Id] = make(map[int]string)
			cars_dregs_time[0][values.Id] = make(map[int]string)
			cars_dregs_time[1][values.Id] = make(map[int]string)
			cars_mask_time[0][values.Id] = make(map[int]string)
			cars_mask_time[1][values.Id] = make(map[int]string)
			cars_safety_helmet_time[0][values.Id] = make(map[int]string)
			cars_safety_helmet_time[1][values.Id] = make(map[int]string)
			cars_roadside_booths_time[0][values.Id] = make(map[int]string)
			cars_roadside_booths_time[1][values.Id] = make(map[int]string)
			cars_emergency_time[0][values.Id] = make(map[int]string)
			cars_emergency_time[1][values.Id] = make(map[int]string)
			carsLaneChangeTime[0][values.Id] = make(map[int]string)
			carsLaneChangeTime[1][values.Id] = make(map[int]string)
			carsStopToRunTime[0][values.Id] = make(map[int]string)
			carsStopToRunTime[1][values.Id] = make(map[int]string)
			carsRunToStopTime[0][values.Id] = make(map[int]string)
			carsRunToStopTime[1][values.Id] = make(map[int]string)
			carsBackTime[0][values.Id] = make(map[int]string)
			carsBackTime[1][values.Id] = make(map[int]string)
		}
		area_points, event_list, scheme_id = scheme_test(values.Id)                                              //解析各检测区域布防
		area_points_all[values.Id], event_list_all[values.Id], scheme_id_all[values.Id] = scheme_test(values.Id) //解析各检测区域布防
	}
	// lane:区域名称,初始化每秒交通流统计表
	for _, values := range streamList {
		Per_second_data_all[values.Id] = make(map[int]map[string][]float64)
		for lane, _ := range area_points_all[values.Id] {
			Per_second_data_all[values.Id][lane] = make(map[string][]float64)
		}
	}
	for lane, _ := range area_points {
		Per_second_data[lane] = make(map[string][]float64)
	}
	return streamList
}

func DetectNew(conn *websocket.Conn, port string, streamList []config.StructStream) {
	var errSocket error
	car_trajectory := map[int][][]float64{} //轨迹数据
	var l_frame string
	frameNumAll := make(map[int]int)
	fmt.Println("开始接收" + port + "数据=========================")
	for {
		//receive
		_, message, err := conn.ReadMessage()
		if err != nil {
			fmt.Println("ReadMessageError:", err)
			time.Sleep(time.Second * 2)
			connectDeepFrameDataNew(port)
			return
		}
		var all_data map[string]string
		json.Unmarshal([]byte(message), &all_data)
		frame_data = string(message)
		port_int, _ := strconv.Atoi(port)
		//大洞山数据处理
		// frame_data_for_web := Func_目标速度(frame_data, port_int, 0)
		// Frame_data_for_web = TrajectoryComplement(frame_data_for_web)

		//
		Frame_data_for_web := Func_目标速度(frame_data, port_int, 0)
		// if time.Now().Hour() < 19 && time.Now().Hour() > 6 {
		l_frame = Frame_data_for_web //帧数据
		l_frame_data := make(map[int]VideoData)
		json.Unmarshal([]byte(l_frame), &l_frame_data)

		// fmt.Println("num: ", l_frame_data)
		for idStream, dataStream := range l_frame_data {
			if frameNumAll[idStream] != dataStream.Frame_num {
				// fmt.Println("num1: ", l_frame)
				// fmt.Println("num2: ", l_frame_data)
				frameNumAll[idStream] = dataStream.Frame_num
				Frame_num = dataStream.Frame_num
				event_save = event_save[0:0]
				if edition == "交通分析版" {
					Compute_flow(0, dataStream, Per_second_data, car_trajectory, Stream_id, scheme_id)
					if Frame_num%(DetectFPS*10) == 0 {
						统计流量(scheme_id, Frame_num, DetectFPS, 10, 0, len(Per_second_data))
						if Frame_num%(DetectFPS*60) == 0 {
							Save_flow(Frame_num, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
						}
					}
				} else {
					Compute_flow(0, dataStream, Per_second_data, car_trajectory, idStream, scheme_id)
				}
				event_to_deep = event_save

				if len(event_to_deep) > 0 {
					eventmsg := map[string]interface{}{"msg_type": "event", "data": event_to_deep}
					dataType, _ := json.Marshal(eventmsg)
					dataString := string(dataType)
					errSocket = conn.WriteMessage(websocket.TextMessage, []byte(dataString))
				}
				if errSocket != nil {
					fmt.Println("WriteMessageError:", errSocket)
					time.Sleep(time.Second * 2)
					connectFrameData2()
					return
				}

				event_flag = 1
			}
			Per_second_data_all[idStream] = Per_second_data
		}
		// 接收占道经营，未戴口罩，未戴安全帽的数据
		if 平台协议.IsSendAiCity {
			var l_frame string
			var l_frame_data map[int]VideoData
			frameNumAll := make(map[int]int) //记录各个流当前帧号，控制重复帧
			l_frame = frame_data3            //口罩检测帧数据
			json.Unmarshal([]byte(l_frame), &l_frame_data)
			for idStream, dataStream := range l_frame_data {
				area_points, event_list, scheme_id = scheme_test(idStream) //各检测区域坐标

				if frameNumAll[idStream] != dataStream.Frame_num {
					frameNumAll[idStream] = dataStream.Frame_num
					event_save = event_save[0:0]
					Compute_mask(0, dataStream, Per_second_data, car_trajectory, idStream)
					event_to_deep = event_save
					event_flag = 1
				}
				Per_second_data_all[idStream] = Per_second_data
			}
			var l_frame2 string
			var l_frame_data2 map[int]VideoData
			frameNumAll2 := make(map[int]int) //记录各个流当前帧号，控制重复帧
			l_frame2 = frame_data5            //安全帽检测帧数据
			json.Unmarshal([]byte(l_frame2), &l_frame_data2)
			for idStream, dataStream := range l_frame_data2 {
				area_points, event_list, scheme_id = scheme_test(idStream) //各检测区域坐标

				if frameNumAll2[idStream] != dataStream.Frame_num {
					frameNumAll2[idStream] = dataStream.Frame_num
					event_save = event_save[0:0]
					Compute_mask(0, dataStream, Per_second_data, car_trajectory, idStream)
					event_to_deep = event_save
					event_flag = 1
				}
				Per_second_data_all[idStream] = Per_second_data
			}
			var l_frame3 string
			var l_frame_data3 map[int]VideoData
			frameNumAll3 := make(map[int]int) //记录各个流当前帧号，控制重复帧
			l_frame3 = frame_data7            //安全帽检测帧数据
			// fmt.Println("占道经营检测帧数据", frame_data7)
			json.Unmarshal([]byte(l_frame3), &l_frame_data3)
			for idStream, dataStream := range l_frame_data3 {
				area_points, event_list, scheme_id = scheme_test(idStream) //各检测区域坐标

				if frameNumAll3[idStream] != dataStream.Frame_num {
					frameNumAll3[idStream] = dataStream.Frame_num
					event_save = event_save[0:0]
					Compute_roadside_booths(0, dataStream, Per_second_data, car_trajectory, idStream)
					event_to_deep = event_save
					event_flag = 1
				}
				Per_second_data_all[idStream] = Per_second_data
			}
		}
		// }
		// time.Sleep(10 * time.Millisecond)
	}
}

func Detect(conn *websocket.Conn) {
	var errSocket error
	scheme_id_all = make(map[int]int)
	area_points_all = make(map[int]map[int][][]int)
	event_list_all = make(map[int]map[int]LaneData)
	Per_second_data_all = make(map[int]map[int]map[string][]float64)

	streamList := config.GetStreamList()
	for _, values := range streamList {
		if values.IsDetected == 1 {
			cars_lowspeed[0][values.Id] = make(map[int]int)
			cars_lowspeed[1][values.Id] = make(map[int]int)
			cars_overspeed[0][values.Id] = make(map[int]int)
			cars_overspeed[1][values.Id] = make(map[int]int)
			cars_people[0][values.Id] = make(map[int]int)
			cars_people[1][values.Id] = make(map[int]int)
			cars_motor[0][values.Id] = make(map[int]int)
			cars_motor[1][values.Id] = make(map[int]int)
			cars_stop[0][values.Id] = make(map[int]int)
			cars_stop[1][values.Id] = make(map[int]int)
			cars_dregs[0][values.Id] = make(map[int]int)
			cars_dregs[1][values.Id] = make(map[int]int)
			cars_mask[0][values.Id] = make(map[int]int)
			cars_mask[1][values.Id] = make(map[int]int)
			cars_safety_helmet[0][values.Id] = make(map[int]int)
			cars_safety_helmet[1][values.Id] = make(map[int]int)
			cars_roadside_booths[0][values.Id] = make(map[int]int)
			cars_roadside_booths[1][values.Id] = make(map[int]int)
			cars_emergency[0][values.Id] = make(map[int]int)
			cars_emergency[1][values.Id] = make(map[int]int)
			carsLaneChange[0][values.Id] = make(map[int]map[string]int)
			carsLaneChange[1][values.Id] = make(map[int]map[string]int)
			carsStopToRun[0][values.Id] = make(map[int]map[string]int)
			carsStopToRun[1][values.Id] = make(map[int]map[string]int)
			carsRunToStop[0][values.Id] = make(map[int]map[string]float64)
			carsRunToStop[1][values.Id] = make(map[int]map[string]float64)
			carsBack[0][values.Id] = make(map[int]map[string]float64)
			carsBack[1][values.Id] = make(map[int]map[string]float64)

			cars_lowspeed_time[0][values.Id] = make(map[int]string)
			cars_lowspeed_time[1][values.Id] = make(map[int]string)
			cars_overspeed_time[0][values.Id] = make(map[int]string)
			cars_overspeed_time[1][values.Id] = make(map[int]string)
			cars_people_time[0][values.Id] = make(map[int]string)
			cars_people_time[1][values.Id] = make(map[int]string)
			cars_motor_time[0][values.Id] = make(map[int]string)
			cars_motor_time[1][values.Id] = make(map[int]string)
			cars_stop_time[0][values.Id] = make(map[int]string)
			cars_stop_time[1][values.Id] = make(map[int]string)
			cars_dregs_time[0][values.Id] = make(map[int]string)
			cars_dregs_time[1][values.Id] = make(map[int]string)
			cars_mask_time[0][values.Id] = make(map[int]string)
			cars_mask_time[1][values.Id] = make(map[int]string)
			cars_safety_helmet_time[0][values.Id] = make(map[int]string)
			cars_safety_helmet_time[1][values.Id] = make(map[int]string)
			cars_roadside_booths_time[0][values.Id] = make(map[int]string)
			cars_roadside_booths_time[1][values.Id] = make(map[int]string)
			cars_emergency_time[0][values.Id] = make(map[int]string)
			cars_emergency_time[1][values.Id] = make(map[int]string)
			carsLaneChangeTime[0][values.Id] = make(map[int]string)
			carsLaneChangeTime[1][values.Id] = make(map[int]string)
			carsStopToRunTime[0][values.Id] = make(map[int]string)
			carsStopToRunTime[1][values.Id] = make(map[int]string)
			carsRunToStopTime[0][values.Id] = make(map[int]string)
			carsRunToStopTime[1][values.Id] = make(map[int]string)
			carsBackTime[0][values.Id] = make(map[int]string)
			carsBackTime[1][values.Id] = make(map[int]string)
		}
		area_points, event_list, scheme_id = scheme_test(values.Id)                                              //解析各检测区域布防
		area_points_all[values.Id], event_list_all[values.Id], scheme_id_all[values.Id] = scheme_test(values.Id) //解析各检测区域布防
	}
	// fmt.Println("-------------------------detect: ", streamList, area_points, scheme_id)
	// edition = 系统.GetEdition()
	if edition != "雷达版" {
		car_trajectory := map[int][][]float64{} //轨迹数据
		var l_frame string
		var l_frame_data map[int]VideoData
		frameNumAll := make(map[int]int)
		if edition == "交通分析版" {
			Stream_id = GetStreamId()
		} //记录各个流当前帧号，控制重复帧

		// lane:区域名称,初始化每秒交通流统计表
		for _, values := range streamList {
			Per_second_data_all[values.Id] = make(map[int]map[string][]float64)
			for lane, _ := range area_points_all[values.Id] {
				Per_second_data_all[values.Id][lane] = make(map[string][]float64)
			}
		}
		for lane, _ := range area_points {
			Per_second_data[lane] = make(map[string][]float64)
		}
		for {
			// if time.Now().Hour() < 19 && time.Now().Hour() > 6 {
			l_frame = Frame_data_for_web //帧数据
			json.Unmarshal([]byte(l_frame), &l_frame_data)
			// fmt.Println("num: ", l_frame_data)
			for idStream, dataStream := range l_frame_data {
				if frameNumAll[idStream] != dataStream.Frame_num {
					// fmt.Println("num1: ", l_frame)
					// fmt.Println("num2: ", l_frame_data)
					frameNumAll[idStream] = dataStream.Frame_num
					Frame_num = dataStream.Frame_num
					event_save = event_save[0:0]
					event_to_deep = event_to_deep[0:0]
					if edition == "交通分析版" {
						Compute_flow(0, dataStream, Per_second_data, car_trajectory, Stream_id, scheme_id)
						if Frame_num%(DetectFPS*10) == 0 {
							统计流量(scheme_id, Frame_num, DetectFPS, 10, 0, len(Per_second_data))
							if Frame_num%(DetectFPS*60) == 0 {
								Save_flow(Frame_num, DetectFPS, len(Per_second_data), scheme_id, 0, Per_second_data)
							}
						}
					} else {
						Compute_flow(0, dataStream, Per_second_data, car_trajectory, idStream, scheme_id)
					}
					event_to_deep = append([]event_data{}, event_save...)
					// fmt.Println("event_save: ", event_save)
					// fmt.Println("event_to_deep: ", event_to_deep)

					if len(event_to_deep) > 0 {
						eventmsg := map[string]interface{}{"msg_type": "event", "data": event_to_deep}
						dataType, _ := json.Marshal(eventmsg)
						dataString := string(dataType)
						errSocket = conn.WriteMessage(websocket.TextMessage, []byte(dataString))
					}
					if errSocket != nil {
						fmt.Println("WriteMessageError:", errSocket)
						time.Sleep(time.Second * 2)
						connectFrameData2()
						return
					}

					event_flag = 1
				}
				Per_second_data_all[idStream] = Per_second_data
			}
			// 接收占道经营，未戴口罩，未戴安全帽的数据
			if 平台协议.IsSendAiCity {
				var l_frame string
				var l_frame_data map[int]VideoData
				frameNumAll := make(map[int]int) //记录各个流当前帧号，控制重复帧
				l_frame = frame_data3            //口罩检测帧数据
				json.Unmarshal([]byte(l_frame), &l_frame_data)
				for idStream, dataStream := range l_frame_data {
					area_points, event_list, scheme_id = scheme_test(idStream) //各检测区域坐标

					if frameNumAll[idStream] != dataStream.Frame_num {
						frameNumAll[idStream] = dataStream.Frame_num
						event_save = event_save[0:0]
						Compute_mask(0, dataStream, Per_second_data, car_trajectory, idStream)
						event_to_deep = event_save
						event_flag = 1
					}
					Per_second_data_all[idStream] = Per_second_data
				}
				var l_frame2 string
				var l_frame_data2 map[int]VideoData
				frameNumAll2 := make(map[int]int) //记录各个流当前帧号，控制重复帧
				l_frame2 = frame_data5            //安全帽检测帧数据
				json.Unmarshal([]byte(l_frame2), &l_frame_data2)
				for idStream, dataStream := range l_frame_data2 {
					area_points, event_list, scheme_id = scheme_test(idStream) //各检测区域坐标

					if frameNumAll2[idStream] != dataStream.Frame_num {
						frameNumAll2[idStream] = dataStream.Frame_num
						event_save = event_save[0:0]
						Compute_mask(0, dataStream, Per_second_data, car_trajectory, idStream)
						event_to_deep = event_save
						event_flag = 1
					}
					Per_second_data_all[idStream] = Per_second_data
				}
				var l_frame3 string
				var l_frame_data3 map[int]VideoData
				frameNumAll3 := make(map[int]int) //记录各个流当前帧号，控制重复帧
				l_frame3 = frame_data7            //安全帽检测帧数据
				// fmt.Println("占道经营检测帧数据", frame_data7)
				json.Unmarshal([]byte(l_frame3), &l_frame_data3)
				for idStream, dataStream := range l_frame_data3 {
					area_points, event_list, scheme_id = scheme_test(idStream) //各检测区域坐标

					if frameNumAll3[idStream] != dataStream.Frame_num {
						frameNumAll3[idStream] = dataStream.Frame_num
						event_save = event_save[0:0]
						Compute_roadside_booths(0, dataStream, Per_second_data, car_trajectory, idStream)
						event_to_deep = event_save
						event_flag = 1
					}
					Per_second_data_all[idStream] = Per_second_data
				}
			}
			// }
			// time.Sleep(10 * time.Millisecond)
		}
	}
}

func Func_车辆轨迹优化(trj_list [][]float64, idStream int, k float64) [][]float64 {
	// trj_list := [][]float64{}
	x := []float64{}
	y := []float64{}
	t := []float64{}
	f := []float64{}
	s := []float64{}
	coo_x := []float64{}
	coo_y := []float64{}
	index := []float64{}
	for i := 0; i < len(trj_list); i++ {
		x = append(x, trj_list[i][0])
		s = append(s, trj_list[i][2])
		t = append(t, trj_list[i][3])
		f = append(f, trj_list[i][4])
		coo_x = append(coo_x, trj_list[i][5])
		coo_y = append(coo_y, trj_list[i][6])
		index = append(index, float64(i))
	}
	// if len(trj_list) > 0 {
	// 	if trj_list[0][1] < trj_list[len(trj_list)-1][1] {
	// 		sort.SliceStable(trj_list, func(i, j int) bool {
	// 			return trj_list[i][1] < trj_list[j][1] //从小到大排列
	// 		})
	// 	} else {
	// 		sort.SliceStable(trj_list, func(i, j int) bool {
	// 			return trj_list[i][1] > trj_list[j][1] //从大到小排列
	// 		})
	// 	}

	// 	// fmt.Println("coox: ", coo_x)
	// 	// if coo_x[0] < coo_x[len(coo_x)-1] {
	// 	// 	sort.SliceStable(coo_x, func(i, j int) bool {
	// 	// 		return coo_x[i] < coo_x[j] //从小到大排列
	// 	// 	})
	// 	// } else {
	// 	// 	sort.SliceStable(coo_x, func(i, j int) bool {
	// 	// 		return coo_x[i] > coo_x[j] //从大到小排列
	// 	// 	})
	// 	// }
	// 	// fmt.Println("------------------------------------------ ")
	// 	// fmt.Println("xxxx: ", coo_x)

	// 	// if coo_y[0] < coo_y[len(coo_y)-1] {
	// 	// 	sort.SliceStable(coo_y, func(i, j int) bool {
	// 	// 		return coo_y[i] < coo_y[j] //从小到大排列
	// 	// 	})
	// 	// } else {
	// 	// 	sort.SliceStable(coo_y, func(i, j int) bool {
	// 	// 		return coo_y[i] > coo_y[j] //从大到小排列
	// 	// 	})
	// 	// }

	// }
	for i := 0; i < len(trj_list); i++ {
		trj_list[i][0] = x[i]
		trj_list[i][2] = s[i]
		trj_list[i][3] = t[i]
		trj_list[i][4] = f[i]
		trj_list[i][5] = coo_x[i]
		trj_list[i][6] = coo_y[i]
		// coo_x = append(coo_x, trj_list[i][5])
		// coo_y = append(coo_y, trj_list[i][6])
	}
	// var delta float64
	// if len(trj_list) > 0 {
	// 	delta = trj_list[0][0]
	// }
	// frame_i := 0
	// for frame_i < len(trj_list) {
	// 	next_frame := 0
	// 	if frame_i+150 < len(trj_list) {
	// 		next_frame = frame_i + 150
	// 	} else {
	// 		next_frame = len(trj_list)
	// 	}
	// 	x = []float64{}
	// 	y = []float64{}
	// 	index = []float64{}
	// 	for i := frame_i; i < next_frame; i++ {
	// 		// if math.Abs(delta-trj_list[i][0]) > 2 {
	// 		// 	trj_list[i][0] = delta*0.7 + trj_list[i][0]*0.3
	// 		// } else {
	// 		// 	delta = trj_list[i][0]
	// 		// }
	// 		x = append(x, trj_list[i][0])
	// 		y = append(y, trj_list[i][1])
	// 		index = append(index, float64(i))
	// 	}
	// 	param_x, _ := UnaryFit(index, x, 2)
	// 	param_y, _ := UnaryFit(index, y, 2)
	// 	// param, _ := UnaryFit(x, y, 2)
	// 	if len(param_x) == 3 {
	// 		index_i := 0
	// 		for i := frame_i; i < next_frame; i++ {
	// 			trj_list[i][0] = param_x[0] + param_x[1]*index[index_i] + param_x[2]*index[index_i]*index[index_i]
	// 			trj_list[i][1] = param_y[0] + param_y[1]*index[index_i] + param_y[2]*index[index_i]*index[index_i]
	// 			// trj_list[i][1] = param[0] + param[1]*trj_list[i][0] + param[2]*trj_list[i][0]*trj_list[i][0]
	// 			trj_list[i][1], _ = strconv.ParseFloat(fmt.Sprintf("%.4f", trj_list[i][1]), 64)
	// 			trj_list[i][0], _ = strconv.ParseFloat(fmt.Sprintf("%.4f", trj_list[i][0]), 64)
	// 			index_i++
	// 		}
	// 	} else {
	// 		continue
	// 	}

	// 	if frame_i+150 >= len(trj_list) {
	// 		break
	// 	} else {
	// 		frame_i = frame_i + 75
	// 	}
	// }
	x = []float64{}
	y = []float64{}
	index = []float64{}
	for i := 0; i < len(trj_list); i++ {
		// if math.Abs(delta-trj_list[i][0]) > 2 {
		// 	trj_list[i][0] = delta*0.7 + trj_list[i][0]*0.3
		// } else {
		// 	delta = trj_list[i][0]
		// }
		x = append(x, trj_list[i][0])
		y = append(y, trj_list[i][1])
		index = append(index, float64(i))
	}
	var changeXorY int
	var param, param_coo logic.Unary
	if math.Abs((y[len(y)-1] - y[0])) > math.Abs((x[len(x)-1] - x[0])) {
		changeXorY = 1
		param, _ = logic.UnaryFit(y, x, 2)
		param_coo, _ = logic.UnaryFit(coo_y, coo_x, 2)
	} else {
		changeXorY = 0
		param, _ = logic.UnaryFit(x, y, 2)
		param_coo, _ = logic.UnaryFit(coo_x, coo_y, 2)
	}
	// param_x, _ := logic.UnaryFit(index, x, 2)
	// param_y, _ := logic.UnaryFit(index, y, 2)
	// // param_coox, _ := logic.UnaryFit(index, coo_x, 2)
	// // param_cooy, _ := logic.UnaryFit(index, coo_y, 2)
	// param, _ := logic.UnaryFit(coo_x, coo_y, 2)
	if len(param) == 3 && len(param_coo) == 3 {
		// testa := []float64{}
		// testa2 := []float64{}
		for i := 0; i < len(trj_list); i++ {

			// testa = append(testa, trj_list[i][5])
			// trj_list[i][0] = param_x[0] + param_x[1]*index[index_i] + param_x[2]*index[index_i]*index[index_i]
			// trj_list[i][1] = param_y[0] + param_y[1]*index[index_i] + param_y[2]*index[index_i]*index[index_i]

			// k := 0.1
			if changeXorY == 1 {
				trj_list[i][0] = trj_list[i][0]*k + (param[0]+param[1]*y[i]+param[2]*y[i]*y[i])*(1-k)
				trj_list[i][5] = trj_list[i][5]*k + (param_coo[0]+param_coo[1]*coo_y[i]+param_coo[2]*coo_y[i]*coo_y[i])*(1-k)
			} else {
				trj_list[i][1] = trj_list[i][1]*k + (param[0]+param[1]*x[i]+param[2]*x[i]*x[i])*(1-k)
				trj_list[i][6] = trj_list[i][6]*k + (param_coo[0]+param_coo[1]*coo_x[i]+param_coo[2]*coo_x[i]*coo_x[i])*(1-k)
			}

			// trj_list[i][5] = param_coox[0] + param_coox[1]*index[index_i] + param_coox[2]*index[index_i]*index[index_i]
			// trj_list[i][6] = param[0] + param[1]*coo_x[i] + param[2]*coo_x[i]*coo_x[i]

			// trj_list[i][5] = Coordinate_transformationV2(trj_list[i][0], trj_list[i][1], Select_matrixV2(strconv.Itoa(idStream), 56))[0]
			// trj_list[i][6] = Coordinate_transformationV2(trj_list[i][0], trj_list[i][1], Select_matrixV2(strconv.Itoa(idStream), 56))[1]

			// trj_list[i][5] = coo_x[i]
			// trj_list[i][6] = coo_y[i]

			// trj_list[i][1] = param[0] + param[1]*trj_list[i][0] + param[2]*trj_list[i][0]*trj_list[i][0]
			trj_list[i][1], _ = strconv.ParseFloat(fmt.Sprintf("%.4f", trj_list[i][1]), 64)
			trj_list[i][0], _ = strconv.ParseFloat(fmt.Sprintf("%.4f", trj_list[i][0]), 64)

			trj_list[i][5], _ = strconv.ParseFloat(fmt.Sprintf("%.4f", trj_list[i][5]), 64)
			trj_list[i][6], _ = strconv.ParseFloat(fmt.Sprintf("%.4f", trj_list[i][6]), 64)

			// testa2 = append(testa2, trj_list[i][5])
		}
		// fmt.Println("1: ", testa)
		// fmt.Println("2: ", testa2)
	} else {
		return trj_list
	}
	return trj_list
}

func smoothPoints(trj_list [][]float64) [][]float64 {
	var n = len(trj_list)
	// var smoothedPoints = make([][]float64, n)
	var a = 3.0 / 14.0

	for i := 0; i < n; i++ {
		if i < 3 || i >= n-3 {
			// smoothedPoints[i] = trj_list[i] // 不做平滑处理
			continue
		} else {
			var sumX, sumY float64
			sumX = a * (trj_list[i-3][5] + trj_list[i+3][5])
			sumX += (4*a + 1.0/7.0) * (trj_list[i-2][5] + trj_list[i+2][5])
			sumX += (6*a + 2.0/7.0) * (trj_list[i-1][5] + trj_list[i+1][5])
			sumX += (7*a + 3.0/7.0) * trj_list[i][5]
			sumY = a * (trj_list[i-3][6] + trj_list[i+3][6])
			sumY += (4*a + 1.0/7.0) * (trj_list[i-2][6] + trj_list[i+2][6])
			sumY += (6*a + 2.0/7.0) * (trj_list[i-1][6] + trj_list[i+1][6])
			sumY += (7*a + 3.0/7.0) * trj_list[i][6]
			trj_list[i][5] = sumX
			trj_list[i][6] = sumY
		}
	}
	// fmt.Println("smooth: ", n)

	return trj_list
}

func MovingAverage2(points [][]float64, windowSize int) [][]float64 {
	// var smoothedPoints [][]float64

	if len(points) < windowSize {
		// 边界情况：不需要平滑，直接返回原始的点
		return points
	}

	for i := 0; i < len(points)-windowSize+1; i++ {

		// 计算窗口内点的x和y坐标的平均值
		sumX0 := 0.0
		sumY0 := 0.0
		sumX1 := 0.0
		sumY1 := 0.0
		for j := i; j < i+windowSize; j++ {
			sumX0 += points[j][5]
			sumY0 += points[j][6]
			sumX1 += points[j][0]
			sumY1 += points[j][1]
		}
		avgX0 := sumX0 / float64(windowSize)
		avgY0 := sumY0 / float64(windowSize)
		avgX1 := sumX1 / float64(windowSize)
		avgY1 := sumY1 / float64(windowSize)

		// 将该平均值作为新点的坐标
		// smoothedPoints = append(smoothedPoints, Point{avgX, avgY})
		points[i][5] = avgX0
		points[i][6] = avgY0
		points[i][0] = avgX1
		points[i][1] = avgY1

	}

	return points
}

func smoothTrajectory(points [][]float64, windowSize int) [][]float64 {
	// 复制一份原始数据，以免修改数据源
	smoothedPoints := make([][]float64, len(points))
	copy(smoothedPoints, points)

	// 计算每个点与其它点的距离，并根据距离给每个点分配权重
	distances := make([][]float64, len(points))
	for i := range distances {
		distances[i] = make([]float64, len(points))
		for j := range distances[i] {
			if i != j {
				distances[i][j] = math.Sqrt(math.Pow((points[i][0]-points[j][0]), 2) + math.Pow((points[i][1]-points[j][1]), 2))
			}
		}
	}

	// 对每个点应用基于距离加权的平滑算法
	for i := 0; i < len(points); i++ {
		var sumX, sumY, sumWeights float64

		// 计算滑动窗口中的加权坐标总和
		for j := 0; j < len(points); j++ {
			if math.Abs(float64(i-j)) <= float64(windowSize)/2 {
				weight := math.Exp(-distances[i][j] / float64(windowSize))
				sumX += points[j][0] * weight
				sumY += points[j][1] * weight
				sumWeights += weight
			}
		}

		// 计算平滑后的坐标值
		if sumWeights > 0 {
			smoothedPoints[i][0] = sumX / sumWeights
			smoothedPoints[i][1] = sumY / sumWeights
		}
	}

	return smoothedPoints
}
